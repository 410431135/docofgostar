####################################################################
GoStar v4.0: F1 Project
####################################################################

.. note::

	- 甚麼叫幸福：白天有說有笑，晚上睡個好覺。
	- 甚麼叫智慧：安排的是能做好，沒安排的事能想到。
	- 甚麼叫EQ：說話讓人喜歡，做事讓人感動，做人讓人想念。
	- 甚麼叫正能量：給人希望，給人方向，給人力量，給人智慧，給人自信，給人快樂。

.. toctree::
	:caption: Part 1: About
	:maxdepth: 5
	:numbered:
	
	About/Author
	About/ChangeLog
	About/Minutes

.. toctree::
	:caption: Part 2: Requirements
	:maxdepth: 6
	:numbered:
	
	Requirements/GanttChart/GanttChart
	Requirements/UseCaseDiagram/UseCaseDiagram
	Requirements/ActivityDiagram/ActivityDiagram
	Requirements/GUIDiagram/GUIDiagram

.. toctree::
	:caption: Part 3: Software Design
	:maxdepth: 6
	:numbered:
	
	SoftwareDesign/SequenceDiagram/SequenceDiagram
	SoftwareDesign/ClassDiagram/ClassDiagram
	SoftwareDesign/EntityRelationshipDiagram/EntityRelationshipDiagram
	SoftwareDesign/ObjectDiagram/ClassDiagram
	SoftwareDesign/DeploymentDiagram/DeploymentDiagram
	