.. note::
	- Start: 20171209
	- System Environment:
		- Python v3.6.3
		- plantuml.jar v1.2018.10
		- graphviz v0.9 (v2.38)
		
####################################################################
Use Case Diagram
####################################################################

********************************************************************
Change Log
********************************************************************
A. v0.1-20190815: Draft by LKY
#. v0.2-20190831: Draft by LKY
#. v0.3-20190902: split subsystems Draft by LKY
#. v0.4-20190903: revise 履歷管理系統、文章管理系統 by LKY

********************************************************************
Diagram
********************************************************************

====================================================================
v0.4-20190903: revise 履歷管理系統、文章管理系統 by LKY
====================================================================

--------------------------------------------------------------------
文章管理系統
--------------------------------------------------------------------

.. uml::

	@startuml
	actor User
	actor System
	actor Manager
	rectangle 文章管理系統{
		User -- (搜尋文章)
		User -- (瀏覽文章)
		(瀏覽文章) <.. (收藏文章) : <<Extends>>
		User --(收藏文章)
		(瀏覽文章) <.. (按讚文章) : <<Extends>>
		User -- (按讚文章)
		(瀏覽文章) <.. (留言文章) : <<Extends>>
		User -- (留言文章)
		(瀏覽文章) <.. (檢舉文章) : <<Extends>>
		User -- (檢舉文章)


		User -- (發表文章)
		User -- (建議新增板塊)
			(建議新增板塊) ..> (發表文章) : <<Extends>>
			(發表文章) <.. (設定閱讀權限) : <<Extends>>
		User -- (設定閱讀權限)
		User -- (投稿文章至電子報)
			(投稿文章至電子報) ..> (計算討論熱度) : <<Include>>
				(計算討論熱度) -- System
			(投稿文章至電子報) ..> (排版成電子報形式) : <<Include>>
				(排版成電子報形式) -- System

		System -- (寄信至審稿人信箱)
			(寄信至審稿人信箱) <.. (投稿文章至電子報) : <<Include>>
		System -- (計算討論熱度)
		System -- (設為熱門文章)
		System -- (分類文章至不同版面)	

		Manager -- (設定文章為置頂)
		Manager -- (新增/變更板塊)
		Manager -- (審查檢舉文章)
			(審查檢舉文章) -- (懲戒使用者) : <<Extends>>
			note right of (懲戒使用者)
				包含警告、扣積分、停權等處分
			end note
	}
	@enduml


--------------------------------------------------------------------
履歷管理系統
--------------------------------------------------------------------

.. uml::

	@startuml
	actor User
	actor 師獎生 as reward
	reward -|> User
	note right of reward
		包含公費生
	end note
	actor Manager
	rectangle 履歷管理系統{
	User -- (管理個人履歷)
	User -- (設定履歷/檔案)
	note right of (設定履歷/檔案)
		包含上傳、編輯、設定隱私
	end note
	User -- (預覽履歷)
	User -- (新增履歷)
		(新增履歷) <.. (複製既有履歷) : <<Extends>>
	reward -- (審查特定履歷)
	User -- (輸出履歷)		
		(管理個人履歷) <.. (設定履歷/檔案) : <<Extends>>
		(管理個人履歷) <.. (輸出履歷) : <<Extends>>
		(管理個人履歷) <.. (預覽履歷) : <<Extends>>
		(管理個人履歷) <.. (新增履歷) : <<Extends>>
	(審查特定履歷) -- Manager
	(設定法規) -- Manager
	(新增特定履歷欄位) -- Manager
	}
	@enduml

====================================================================
v0.3-20190902: split subsystems Draft by LKY
====================================================================

--------------------------------------------------------------------
寶物系統
--------------------------------------------------------------------

.. uml::

	@startuml
	left to right direction
	actor User
	actor System
	actor Manager
	rectangle 寶物系統{
		User -- (管理寶物)
		User -- (拼蓋寶物)
		note right of (拼蓋寶物)
			在畫布上蓋積木
		end note
		User -- (交換寶物)
		System -- (派發例行任務給使用者)
		note right of (派發例行任務給使用者)
			例如 每日任務
		end note
		User -- (查看任務/成就)
		System -- (派發寶物給使用者)
		note right of (派發寶物給使用者)
			當使用者完成任務、達到成就、達到文章或留言等的規定門檻等等
			可得到寶物
		end note

		Manager -- (自定義任務給使用者)
		note right of (自定義任務給使用者)
			管理員可依照需要自訂任務給使用者完成
		end note
	}
	@enduml

--------------------------------------------------------------------
好友訊息系統
--------------------------------------------------------------------

.. uml::
	
	@startuml
	actor User
	actor 文章管理系統 as	artical
	rectangle 好友訊息系統{
		User -- (設立活動群組)
		note right of (設立活動群組)
			例如 研習活動、史懷哲、數位學伴等
		end note
			(設立活動群組) <.. (發表活動成果) : <<Extends>>
				(發表活動成果) -- artical
			(設立活動群組) <.. (加入活動成員) : <<Extends>>
			(設立活動群組) <.. (線上討論) : <<Extends>>

		User -- (新增/刪除好友)
		User -- (與好友線上聊天)
		User -- (瀏覽好友資訊)
		note right of (瀏覽好友資訊)
			如 個人履歷、寶物等
		end note
		User -- (與好友交換寶物)
	}
	@enduml

--------------------------------------------------------------------
權限管理系統
--------------------------------------------------------------------

.. uml::

	@startuml
	actor User 
	actor TopManager
	actor Manager
	actor System
	TopManager -|> Manager
	rectangle 權限管理系統{
		User -- (新增/異動身份)
		note right of (新增/異動身份)
			使用者可以新增自己為 師資生、師獎生、公費生、教師等身份
			審查通過之後，即獲得該身份所有之權利及欄位等等
		end note
		Manager -- (審查使用者身份)
		TopManager -- (管理管理員之權限)

		System -- (計算使用者閱讀權限)
		note right of  (計算使用者閱讀權限)
			根據「寶物」規則計算並賦予對應之閱讀權限
		end note
	}
	@enduml

--------------------------------------------------------------------
文章管理系統
--------------------------------------------------------------------

.. uml::

	@startuml
	actor User
	actor System
	actor Manager
	rectangle 文章管理系統{
		User -- (瀏覽文章)
		(瀏覽文章) <.. (收藏文章) : <<Extends>>
		User --(收藏文章)
		(瀏覽文章) <.. (按讚文章) : <<Extends>>
		User -- (按讚文章)
		(瀏覽文章) <.. (留言文章) : <<Extends>>
		User -- (留言文章)
		(瀏覽文章) <.. (檢舉文章) : <<Extends>>
		User -- (檢舉文章)


		User -- (發表文章)
			(發表文章) <.. (設定閱讀權限) : <<Extends>>
		User -- (設定閱讀權限)
		User -- (投稿文章至電子報)
			(投稿文章至電子報) ..> (計算討論熱度) : <<Include>>
				(計算討論熱度) -- System
			(投稿文章至電子報) ..> (排版成電子報形式) : <<Include>>
				(排版成電子報形式) -- System

		System -- (寄信至審稿人信箱)
			(寄信至審稿人信箱) <.. (投稿文章至電子報) : <<Include>>
		System -- (計算討論熱度)
		System -- (設為熱門文章)
		System -- (分類文章至不同版面)	

		Manager -- (設定文章為置頂)
		Manager -- (新增/變更板塊)
		Manager -- (審查檢舉文章)
			(審查檢舉文章) -- (懲戒使用者) : <<Extends>>
			note right of (懲戒使用者)
				包含警告、扣積分、停權等處分
			end note
	}
	@enduml

--------------------------------------------------------------------
履歷管理系統
--------------------------------------------------------------------

.. uml::
	
	@startuml
	actor User
	actor 師獎生 as reward
	reward -|> User
	note right of reward
		包含公費生
	end note
	actor Manager
	rectangle 履歷管理系統{
	User -- (管理個人履歷)
	User -- (上傳履歷/檔案)
	note right of (上傳履歷/檔案)
		一般生
	end note
	reward -- (上傳需審查的履歷/檔案)
		(上傳履歷/檔案) <.. (上傳需審查的履歷/檔案) : <<Extends>> 
	User -- (編輯檔案)
	User -- (設定隱私)
	note right of (設定隱私)
		設定哪些履歷呈現/隱藏
	end note
	User -- (輸出履歷)		
		(管理個人履歷) <.. (上傳履歷/檔案) : <<Extends>>			
		(管理個人履歷) <.. (編輯檔案) : <<Extends>>			
		(管理個人履歷) <.. (設定隱私) : <<Extends>>			
		(管理個人履歷) <.. (輸出履歷) : <<Extends>>
	(審查特定履歷) -- Manager
	(設定法規) -- Manager
	}
	@enduml


====================================================================
v0.2-20190831: Draft by LKY
====================================================================
.. uml::

	@startuml

	actor User
	actor Manager
	actor TopManager
	actor DataBase as db
	actor 好友訊息系統 as mes
	actor 個人履歷系統 as cv
	actor 寶物系統 as reward
	actor 文章管理系統 as artical
	actor 權限管理系統 as authority
	actor 檢舉管理系統 as abuse
	actor 任務系統 as mission
	cv -- db
	artical -- db
	TopManager -|> Manager

	rectangle FrontEnd{
		User -- (管理身份)
			(管理身份) -- authority
		User -- (管理個人履歷)
			(管理個人履歷) -- cv
			(管理個人履歷) <.. (上傳檔案) : <<Extends>>
				(上傳檔案) -- cv
			(管理個人履歷) <.. (編輯檔案) : <<Extends>>
				(編輯檔案) -- cv
			(管理個人履歷) <.. (設定顯示) : <<Extends>>
				(設定顯示) -- cv
			(管理個人履歷) <.. (輸出履歷) : <<Extends>>
				(輸出履歷) -- cv

		User -- (管理寶物)
			(管理寶物) -- reward
		User -- (查看任務)
			(查看任務) -- mission

		User -- (查看文章)
			(查看文章) -- artical
			(查看文章) <.. (收藏文章) : <<Extends>>
				(收藏文章) -- artical
			(查看文章) <.. (按讚文章) : <<Extends>>
				(按讚文章) -- artical
			(查看文章) <.. (留言文章) : <<Extends>>
				(留言文章) -- artical
			(查看文章) <.. (檢舉文章) : <<Extends>>
				(檢舉文章) -- abuse

		User -- (發表文章)
			(發表文章) -- artical
			(發表文章) -- (設定閱讀權限)
				(設定閱讀權限) -- authority
		User -- (投稿文章至電子報)
			(投稿文章至電子報) ..> (計算討論熱度) : <<Include>>
			(投稿文章至電子報) ..> (排版成電子報形式) : <<Include>>

		artical -- (計算討論熱度)
		artical -- (設為熱門文章)
		artical -- (分類文章至不同版面)	

		User -- (設立專案群組)
			(設立專案群組) -- mes
			(設立專案群組) -- (加入成員) : <<Extends>>
			(設立專案群組) -- (線上討論) : <<Extends>>
			(設立專案群組) -- (發表成果)	 : <<Extends>>
				(發表成果) -- artical

		


	}
	rectangle BackEnd{
		TopManager -- (管理管理員權限)
			(管理管理員權限) -- authority

		Manager -- (審查檢舉)
		(審查檢舉) -- abuse

		mission -- (派發任務給使用者)
	}
	@enduml

====================================================================
v0.1-20190815: Draft by LKY
====================================================================
A. Overview

	.. image:: 師就論壇-20190815-lgy.png
		:scale: 100 %
		:alt: Teacher Program: Discussion Forum
		:align: center

#. Some Thoughts

	.. image:: thought-20190816-lgy.png
		:scale: 100 %
		:alt: Thought
		:align: center

	.. image:: thought2-20190816-lgy.png
		:scale: 100 %
		:alt: Thought 2
		:align: center

#. Minutes: 
	A. 論壇research
		A. categories management:
			– (Admin) add section / rules
		#. rules:
			– user have to accept the all the rules
			– user have to provide informations, and the privacy would be guarentee
			– user can have nickname but may be delete if which violates the rules.
			– no repeated registration
			– not activity for a long time may be removed
		#. conduct:
			– any conduction is based on morality and netiquette
			– no sewear nor abusive words, including the sustitution ones.
			– no commercial Ads except Admin agrees.
			– signature rules.
		#. message:
			– identical threads in different place is prohibited.
	#. Admin reserves right to change the rules.
		#. setting:
			– days during which users can edit & remove their threads/post.
			– classed as hot after numbers of messages (dicussed).
			– max message length (e.g 5000)
		#. limitations:
			– files & images
		#. Notifications:
		#. Users’ setting (backend):
			– list all users info.
			– (user can) log with soical media (e.g Google)
			– ranks
			– avatars (aka character/role)
			– privacy & cookie policy
			– user agreement
			– comments editor (B I U … )
		#. tools:
			– awards
		#. security:
			– number of simultaneous logins.
			– session timeout.
			– protection from website cloning.


	