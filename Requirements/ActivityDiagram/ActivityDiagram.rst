.. note::
	- Start: 20171209
	- System Environment:
		- Python v3.6.3
		- plantuml.jar v1.2018.10
		- graphviz v0.9 (v2.38)
		
####################################################################
Activity Diagram
####################################################################


********************************************************************
Change Log
********************************************************************
A. v0.1-20190905: Draft by LKY
#. v0.2-20190905: edited after meeting
#. v0.3-20190915: add 瀏覽文章、活動群組討論及發表

********************************************************************
Diagrams
********************************************************************

====================================================================
v0.1-20190905: Draft by LKY
====================================================================

1. :ref:`Act_title0`
	A. :ref:`Act_graph0_1`
	#. :ref:`Act_graph0_2`
	#. :ref:`Act_graph0_3`
	#. :ref:`Act_graph0_4`
#. :ref:`Act_title1`
	A. :ref:`Act_graph1_1`
	#. :ref:`Act_graph1_2`
#. :ref:`Act_title2`
	A. :ref:`Act_graph2_1`
	#. :ref:`Act_graph2_2`
#. :ref:`Act_title3`
	A. :ref:`Act_graph3_1`
	#. :ref:`Act_graph3_2`
	#. :ref:`Act_graph3_3`
	#. :ref:`Act_graph3_4`
#. :ref:`Act_title4`

.. _Act_title0:

--------------------------------------------------------------------
好友訊息系統
--------------------------------------------------------------------

.. _Act_graph0_1:

^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
設立活動群組
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. uml::

	@startuml
	start
	:進入好友訊息頁面;
	:點擊設立活動群組按鈕;
	
	repeat
		:設定活動群組資料;
		note right
			包含名稱、日期、描述、
			隱私(公開、半公開、非公開)、
			審查加入成員、有效期限等等
		end note
	repeat while (存在相同名稱的群組)
	
	:進入活動群組;
	stop
	@enduml

.. _Act_graph0_2:

^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
加入活動成員
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. uml::
	
	@startuml
	start
	:進入活動群組頁面;
	
	while (欲加入成員) is (Yes)
		:點擊加入活動成員按鈕;
		:選擇加入方式;
		if (搜尋使用者名稱) then (Yes)
			:按下送出邀請按鈕;
		else (No)
			:複製連結;
			:發送網址;
		endif
	endwhile (No)
	
	stop
	@enduml

.. note::
	
	#. 原 從好友選擇 -> 搜尋所有使用者(名稱)

.. _Act_graph0_3:

^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
加入活動群組
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. uml::
	
	@startuml
	|User|
	start
	
	if (收到群組邀請) then (Yes)
		if (點擊接受) then
			:成功加入活動群組;
			stop
		else (拒絕)
			end
		endif
	elseif (點擊群組邀請網址) then (Yes)
		if (點擊接受) then
			:成功加入活動群組;
			stop
		else (拒絕)
			end
		endif
	else (neither)
		:進入活動群組版塊;
		:搜尋欲加入的群組;
		:點擊申請加入按鈕>
		|group|
		:收到申請加入要求<
		if (同意) then (Yes)
			|User|
			:成功加入;
			stop
		else (No)
			|User|
			:收到拒絕加入通知<
			end
		endif
	endif
	
	@enduml

.. _Act_graph0_4:

^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
活動群組討論及發表
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. uml::

	@startuml
	start
	:進入活動群組頁面;
	if (想設定群組管理員) then (Yes)
		:設定群組管理員;
	elseif (想發表便利貼) then (Yes)
		:於便利貼打字並送出;
	elseif (想發言) then (Yes)
		:於聊天窗發言;
	elseif (想新增討論串) then (Yes)
		:點擊新增討論串、子標題;
	elseif (想編輯活動群組資訊) then (Yes)
		:編輯活動群組資訊;
		note right
			資訊如 設定活動群組
		end note
	elseif (想加入成員) then (Yes)
		:加入活動成員;
		note right
			參照 加入活動成員
		end note
	elseif (便利貼加入討論主題) then (Yes)
		:將便利貼加入討論主題;
	else (其他動作)
	endif
	stop
	@enduml

.. note::
	
	#. 加入「將便利貼加入討論主題」
	#. 20190918

.. _Act_title1:

--------------------------------------------------------------------
履歷管理系統
--------------------------------------------------------------------
.. _Act_graph1_1:

^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
審查特定履歷
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. uml::
	
	@startuml
	|User|
	start
	:勾選欲審查的履歷;
	:送出審查>
	|Manager|
	:收到審查履歷請求<
	:進入審查介面;
	
	if (履歷屬實) then (Yes)
		:核發認證>
		stop
	else (No)
		fork
		:駁回申請;
		fork again
		:通知使用者提交複查>
		note right
			使用者同一項目
			Ｎ天內禁止送出審查
		end note
		end fork
		stop
	endif
	
	@enduml

.. _Act_graph1_2:

^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
設定履歷
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. uml::
	
	@startuml
	start
	
	if (尚無任何履歷/欲新增履歷) then (Yes)
		:新增一份履歷;
	else (已有履歷)
		:選擇欲編輯履歷;
	endif

	while (欲新增履歷內容) is (True)
	:於欄位中填寫或上傳檔案;
	endwhile (False)
	
	:設定履歷隱私;
	floating note right
		可設定單筆或整個欄位、整個履歷
	end note
	:儲存履歷;
	note right
		停留在編輯頁面，不跳轉
	end note
	stop
	@enduml

.. note::
	
	#. rewritten 20190909
	#. 未定案「總履歷」存在與否
	#. 20190909
	#. 確定使用「總履歷」

.. _Act_title2:

--------------------------------------------------------------------
權限管理系統
--------------------------------------------------------------------

.. _Act_graph2_1:

^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
管理員權限異動
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. uml::

	@startuml
	|TopManager|
	start
	
	if (選擇單一個管理員) then (Yes)
	else (else)
		:選擇管理員群組;
	endif
	
	:選擇欲更動之權限;
	:完成更動;
	stop
	@enduml

.. note::
	
	#. changed 20190909

.. _Act_graph2_2:

^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
使用者身份異動
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
使用者身份異動-全

.. uml::

	@startuml
	|User|
	start
	:點選新增或異動身份按鈕;
	
	if (選擇一般學生) then (Yes)
	elseif (選擇校友) then (Yes)
	note left
		校友五年內，提供畢業證書
	end note
	elseif (選擇師資生) then (Yes)
	elseif (選擇師獎生) then (Yes)
	elseif (選擇公費生) then (Yes)
	elseif (選擇教師) then (Yes)
	elseif (選擇行政人員) then (Yes)
	else (else)
		:選擇社會人士;
	note right
		身分證明為佐證
	end note
	endif
	
	:選填該身份年限;
	:上傳佐證;
	:送出等待管理員審查>
	(A)
	detach
	|Manager|
	(A)
	:收到身份異動通知<
	
	if (該使用者身份屬實) then (Yes)
		:通過申請;
		|User|
		:取得該身份之權限<
		stop
	else (No)
		|Manager|
		:駁回申請>
		stop
		
	@enduml

使用者身份異動-送出等待審查

.. uml::

	@startuml
	title User
	start
	:點選新增或異動身份按鈕;
	
	if (選擇一般學生) then (Yes)
	elseif (選擇校友) then (Yes)
	note left
		校友五年內，提供畢業證書
	end note
	elseif (選擇師資生) then (Yes)
	elseif (選擇師獎生) then (Yes)
	elseif (選擇公費生) then (Yes)
	elseif (選擇教師) then (Yes)
	elseif (選擇行政人員) then (Yes)
	else (else)
		:選擇社會人士;
	note right
		身分證明為佐證
	end note
	endif
	
	:選填該身份年限;
	:上傳佐證;
	:送出等待管理員審查>
	stop
	@enduml

使用者身份異動-管理員審查

.. uml::
	
	@startuml
	|Manager|
	start
	:收到身份異動通知<
	if (該使用者身份屬實) then (Yes)
		:通過申請;
		|User|
		:取得該身份之權限<
		stop
	else (No)
		|Manager|
		:駁回申請>
		stop
	@enduml

.. note::

	#. 新增一般學生
	#. 新增校友
	#. 新增社會人士
	#. edited 20190909
	#. 拆為兩張圖
	#. edited 20190916

.. _Act_title3:

--------------------------------------------------------------------
文章管理系統
--------------------------------------------------------------------

.. _Act_graph3_1:

^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
管理員置頂文章
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. uml::

	@startuml
	start
	:進入文章管理頁瀏覽文章;
	
	if (欲置頂的文章已存在) then (Yes)
		:勾選為置頂;
	else (No)
		:新增置頂文章;
		:撰寫置頂文章;
		:發表文章;
	endif
	
	:文章置頂;
	stop
	@enduml

.. _Act_graph3_2:

^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
使用者發表文章
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. uml::

	@startuml
	start
	:點擊發表文章按鈕;
	
	if (有之前未完成的文章) then (Yes)
		:繼續編輯文章內容;
	else (No)
		:開始編輯文章內容;
	endif
	
	repeat
		fork
			:設定一般文章設定;
			note right
				包含：
				發表至哪個板塊、文章標題、發文身份
			end note
		fork again
			:設定閱讀權限;
		fork again
			:編輯內文;
		fork again
			:預覽文章;
		fork again
			:查看發文規則;
		end fork
		
		:點擊下一步按鈕;
	repeat while (有欄位為空)
		
	:編輯話題並發布;
	:跳轉至該篇文章位置;
	stop
	@enduml

.. _Act_graph3_3:

^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
檢舉文章
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
檢舉文章-總圖

.. uml::

	@startuml
	|檢舉人|
	start
	:點選檢舉按鈕;
	:選擇檢舉分類;
	:輸入檢舉原因;
	:<<Sending Singal>>
	送出檢舉>
	fork
	|Manager|
	:收到檢舉通知<
	fork again
	|被檢舉人|
	:收到檢舉通知<
	fork again
	|檢舉人|
	:收到檢舉成功送出通知<
	note right
		僅代表成功送出檢舉
		並不代表檢舉屬實
	end note
	end
	
	end fork
	|Manager|
	if (檢舉屬實) then (Yes)
		:填寫檢舉屬實原因;
		:懲戒使用者;
	else (No)
		:填寫檢舉駁回原因;
	endif
	fork
	|檢舉人|
	:得到檢舉結果<
	fork again
	|被檢舉人|
	:得到檢舉結果<
	end fork
	if (對檢舉或處分結果有異議) then (Yes)
		:提交複查>
		|Manager|
		:進行複查;
		if (檢舉結果) then (不變)
			:維持處分;
		else (有更動)
			:變更處分;
		endif
		:通知(被)檢舉人>
		floating note right
			本系統應僅讓被檢舉人複查乙次
			而審查人應讓處分小於或等於原處分
			若仍有異議則應提交相關證明寄至管理員
		end note
		stop
	else (No)
		:被檢舉人確認結案;
	endif
	stop
	@enduml

檢舉文章-檢舉人
	
.. uml::
	
	@startuml
	start
	:點選檢舉按鈕;
	:選擇檢舉分類;
	:輸入檢舉原因;
	:送出檢舉>
	(B)
	detach
	(C)
	:收到檢舉成功送出通知<
	:得到檢舉結果<
	stop
	@enduml

檢舉文章-管理員

.. uml::
	
	@startuml
	(B)
	:收到檢舉通知<
	if (檢舉屬實) then (Yes)
		:填寫檢舉屬實原因;
		:懲戒使用者;
	else (No)
		:填寫檢舉駁回原因;
	endif
	(C)
	detach

	(D)
	:進行複查;
	if (檢舉結果) then (不變)
		:維持處分;
	else (有更動)
		:更動處分;
	endif
	:通知（被）檢舉人;
	(E)
	stop
	@enduml

檢舉文章-被檢舉人

.. uml::

	@startuml
	(B)
	:收到檢舉通知<
	detach
	(C)
	:得到檢舉結果<
	if (對檢舉或處分結果有異議) then (Yes)
		:提交複查;
		(D)
		detach
		(E)
		:得到複查結果<
	else (No)
		:被檢舉人確認結案;
	endif
	stop
	@enduml

.. note::
	#. 拆成三張圖
	#. edited 20190916

.. _Act_graph3_4:
	
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
瀏覽文章
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. uml::
	
	@startuml
	start
	:瀏覽文章;
	if (想留言) then (Yes)
		:撰寫留言;
		:送出;
	elseif (想收藏) then (Yes)
		:點擊收藏文章;
	elseif (想按讚) then (Yes)
		:點讚;
	elseif (想檢舉) then (Yes)
		:點擊檢舉文章;
		note right
			流程參考 檢舉文章
		end note
	elseif (想分享) then (Yes)
		:點擊分享文章;

	else (else)
		:其他動作;
	endif
	stop
	@enduml

.. note::
	
	#. 刪除「刪除文章」功能
	#. 刪除「編輯文章」功能
	#. 20190916

.. _Act_title4:

--------------------------------------------------------------------
登入
--------------------------------------------------------------------

.. uml::

	@startuml
	start
	:點擊身份星球;
	if (已登入) then (Yes)
		:進入該身份板塊頁面;
		stop
	else (No)
		repeat
			:進入登入頁面;
			:輸入單登帳密;
			:點擊登入按鈕;		
		repeat while (驗證帳密) is (不通過)
		:進入該身份板塊頁面;
		stop
	@enduml

.. note::
	
	#. 未定
	#. 使用單登或註冊新帳號
	#. 若用單登分成三種頁面
		#. 學生
		#. 教師
		#. 校外
