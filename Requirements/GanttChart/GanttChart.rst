.. note::
	- Start: 20180908
	- System Environment:
		- Python v3.6.3
		- plantuml.jar v1.2018.10
		- graphviz v0.9 (v2.38)
			
####################################################################
Gantt Chart
####################################################################

********************************************************************
Change Log
********************************************************************
A. v0.1-20190827: Draft by CPH
#. v0.2-20190827: Revised after 2nd meeting by CPH

********************************************************************
Diagram
********************************************************************

====================================================================
v0.2-20190827: Revised after 2nd meeting by CPH
====================================================================
A. English Version

	.. uml::
				
		@startgantt
			Project starts the 15th of august 2019
			saturday are closed
			sunday are closed
			
			[RA: Use Case Diagram] lasts 13 days
			[RA: Use Case Diagram] is colored in Lavender/LightBlue

			[RA: Sphinx Built] starts the 18th of august 2019
			[RA: Sphinx Built] lasts 5 days
			[RA: Sphinx Built] is colored in Lavender/LightBlue
			
			[RA: Gantt Chart] starts at [RA: Sphinx Built]'s end
			[RA: Gantt Chart] lasts 6 days
			[RA: Gantt Chart] is colored in Lavender/LightBlue

			[Project Scope Fixed] happens at [RA: Gantt Chart]'s end
			-- Phase 1: Requirements Analysis --

			[RA: Activity Diagram] starts 7 days before [RA: Use Case Diagram]'s end
			[RA: Activity Diagram] lasts 10 days
			[RA: Activity Diagram] is colored in Lavender/LightBlue

			[RA: GUI Diagram] starts 7 days before [RA: Use Case Diagram]'s end
			[RA: GUI Diagram] lasts 10 days
			[RA: GUI Diagram] is colored in Lavender/LightBlue

			[RA: Sequence Diagram] starts 7 days before [RA: Use Case Diagram]'s end
			[RA: Sequence Diagram] lasts 10 days
			[RA: Sequence Diagram] is colored in Lavender/LightBlue
			
			[Requirements Fixed] happens at [RA: Activity Diagram]'s end
			-- Phase 2: Software Design --
			
			[SD: Deployment Diagram] starts 2 days before [RA: Sequence Diagram]'s end
			[SD: Deployment Diagram] lasts 5 days
			[SD: Deployment Diagram] is colored in Coral/Yellow
			
			[SD: Class Diagram] starts 2 days before [RA: Sequence Diagram]'s end
			[SD: Class Diagram] lasts 10 days
			[SD: Class Diagram] is colored in Coral/Yellow

			[SD: Entity Relationship Diagram] starts at [SD: Class Diagram]'s end
			[SD: Entity Relationship Diagram] lasts 5 days
			[SD: Entity Relationship Diagram] is colored in Coral/Yellow

			[SD: Object Diagram (Optional)] starts at [SD: Class Diagram]'s end
			[SD: Object Diagram (Optional)] lasts 5 days
			[SD: Object Diagram (Optional)] is colored in Coral/Yellow
			
			[SD: Component Diagram (Optional)] starts at [SD: Class Diagram]'s end
			[SD: Component Diagram (Optional)] lasts 5 days
			[SD: Component Diagram (Optional)] is colored in Coral/Yellow

			[Design Fixed] happens at [SD: Entity Relationship Diagram]'s end
			-- Phase 3: Software Development --
			
			[Dev: System Environment Built] starts at [SD: Entity Relationship Diagram]'s end
			[Dev: System Environment Built] lasts 5 days
			[Dev: System Environment Built] is colored in Coral/Yellow

			[Dev: Programming] starts at [Dev: System Environment Built]'s end
			[Dev: Programming] lasts 15 days
			[Dev: Programming] is colored in Coral/Yellow

			[Code Completed] happens at [Dev: Programming]'s end
			-- Phase 4: Software Testing --

			[ST: Unit Test] starts at [Dev: Programming]'s end
			[ST: Unit Test] lasts 5 days
			[ST: Unit Test] is colored in Coral/LightBlue

			[ST: Integrated Test] starts at [ST: Unit Test]'s end
			[ST: Integrated Test] lasts 5 days
			[ST: Integrated Test] is colored in Coral/LightBlue

			[System Fixed] happens at [ST: Integrated Test]'s end
			-- Phase 5: Software Depolyment --

			[SM: System Deployment] starts at [ST: Integrated Test]'s end
			[SM: System Deployment] lasts 5 days
			[SM: System Deployment] is colored in Coral/LightRed

			[SM: Training] starts at [SM: System Deployment]'s end
			[SM: Training] lasts 5 days
			[SM: Training] is colored in Coral/LightRed
		@endgantt

====================================================================
v0.1-20190827: Draft by CPH
====================================================================
A. English Version

	.. uml::
				
		@startgantt
			Project starts the 15th of august 2019
			[RA: Use Case Diagram] lasts 21 days
			[RA: Use Case Diagram] is colored in Lavender/LightBlue

			[Project Scope Fixed] happens at [RA: Use Case Diagram]'s end

			[RA: Sphinx Built] starts the 18th of august 2019
			[RA: Sphinx Built] lasts 7 days
			[RA: Sphinx Built] is colored in Lavender/LightBlue
			
			[RA: Gantt Chart] starts at [RA: Sphinx Built]'s end
			[RA: Gantt Chart] lasts 14 days
			[RA: Gantt Chart] is colored in Lavender/LightBlue

			[RA: Activity Diagram] starts at [RA: Use Case Diagram]'s end
			[RA: Activity Diagram] lasts 7 days
			[RA: Activity Diagram] is colored in Lavender/LightBlue

			[RA: Sequence Diagram] starts at [RA: Use Case Diagram]'s end
			[RA: Sequence Diagram] lasts 7 days
			[RA: Sequence Diagram] is colored in Lavender/LightBlue
			
			[Requirements Fixed] happens at [RA: Activity Diagram]'s end
			
			[SD: Deployment Diagram] starts at [RA: Sequence Diagram]'s end
			[SD: Deployment Diagram] lasts 7 days
			[SD: Deployment Diagram] is colored in Coral/Green
			
			[SD: Class Diagram] starts at [RA: Sequence Diagram]'s end
			[SD: Class Diagram] lasts 14 days
			[SD: Class Diagram] is colored in Coral/Green

			[SD: Entity Relationship Diagram] starts at [SD: Class Diagram]'s end
			[SD: Entity Relationship Diagram] lasts 7 days
			[SD: Entity Relationship Diagram] is colored in Coral/Green

			[SD: Object Diagram (Optional)] starts at [SD: Class Diagram]'s end
			[SD: Object Diagram (Optional)] lasts 7 days
			[SD: Object Diagram (Optional)] is colored in Coral/Green
			
			[SD: Component Diagram (Optional)] starts at [SD: Class Diagram]'s end
			[SD: Component Diagram (Optional)] lasts 7 days
			[SD: Component Diagram (Optional)] is colored in Coral/Green

			[Design Fixed] happens at [SD: Entity Relationship Diagram]'s end
			
			[Dev: System Environment Built] starts at [SD: Entity Relationship Diagram]'s end
			[Dev: System Environment Built] lasts 7 days
			[Dev: System Environment Built] is colored in Coral/Yellow

			[Dev: Programming] starts at [Dev: System Environment Built]'s end
			[Dev: Programming] lasts 21 days
			[Dev: Programming] is colored in Coral/Yellow

			[ST: Unit Test] starts at [Dev: Programming]'s end
			[ST: Unit Test] lasts 7 days
			[ST: Unit Test] is colored in Coral/LightBlue

			[ST: Integrated Test] starts at [ST: Unit Test]'s end
			[ST: Integrated Test] lasts 7 days
			[ST: Integrated Test] is colored in Coral/LightBlue

			[System Fixed] happens at [ST: Integrated Test]'s end

			[SM: System Deployment] starts at [ST: Integrated Test]'s end
			[SM: System Deployment] lasts 7 days
			[SM: System Deployment] is colored in Coral/LightRed

			[SM: Training] starts at [SM: System Deployment]'s end
			[SM: Training] lasts 7 days
			[SM: Training] is colored in Coral/LightRed
		@endgantt

#. Chinese Version

	.. uml::
				
		@startgantt
			Project starts the 15th of august 2019
			[RA: 使用案例圖] lasts 21 days
			[RA: 使用案例圖] is colored in Lavender/LightBlue

			[專案範圍凍結] happens at [RA: 使用案例圖]'s end

			[RA: 文件系統] starts the 18th of august 2019
			[RA: 文件系統] lasts 7 days
			[RA: 文件系統] is colored in Lavender/LightBlue
			
			[RA: 專案排程] starts at [RA: 文件系統]'s end
			[RA: 專案排程] lasts 14 days
			[RA: 專案排程] is colored in Lavender/LightBlue

			[RA: 活動圖] starts at [RA: 使用案例圖]'s end
			[RA: 活動圖] lasts 7 days
			[RA: 活動圖] is colored in Lavender/LightBlue

			[RA: 循序圖] starts at [RA: 使用案例圖]'s end
			[RA: 循序圖] lasts 7 days
			[RA: 循序圖] is colored in Lavender/LightBlue
			
			[需求凍結] happens at [RA: 活動圖]'s end
			
			[SD: 佈建圖] starts at [RA: 循序圖]'s end
			[SD: 佈建圖] lasts 7 days
			[SD: 佈建圖] is colored in Coral/Green
			
			[SD: 類別圖] starts at [RA: 循序圖]'s end
			[SD: 類別圖] lasts 14 days
			[SD: 類別圖] is colored in Coral/Green

			[SD: 實體關係圖] starts at [SD: 類別圖]'s end
			[SD: 實體關係圖] lasts 7 days
			[SD: 實體關係圖] is colored in Coral/Green

			[SD: 物件圖 (可選)] starts at [SD: 類別圖]'s end
			[SD: 物件圖 (可選)] lasts 7 days
			[SD: 物件圖 (可選)] is colored in Coral/Green
			
			[SD: 元件圖 (可選)] starts at [SD: 類別圖]'s end
			[SD: 元件圖 (可選)] lasts 7 days
			[SD: 元件圖 (可選)] is colored in Coral/Green

			[設計凍結] happens at [SD: 實體關係圖]'s end
			
			[Dev: 建置系統環境] starts at [SD: 實體關係圖]'s end
			[Dev: 建置系統環境] lasts 7 days
			[Dev: 建置系統環境] is colored in Coral/Yellow

			[Dev: 程式撰寫] starts at [Dev: 建置系統環境]'s end
			[Dev: 程式撰寫] lasts 21 days
			[Dev: 程式撰寫] is colored in Coral/Yellow

			[ST: 單元測試] starts at [Dev: 程式撰寫]'s end
			[ST: 單元測試] lasts 7 days
			[ST: 單元測試] is colored in Coral/LightBlue

			[ST: 整合測試] starts at [ST: 單元測試]'s end
			[ST: 整合測試] lasts 7 days
			[ST: 整合測試] is colored in Coral/LightBlue

			[系統凍結] happens at [ST: 整合測試]'s end

			[SM: 系統佈建] starts at [ST: 整合測試]'s end
			[SM: 系統佈建] lasts 7 days
			[SM: 系統佈建] is colored in Coral/LightRed

			[SM: 教育訓練] starts at [SM: 系統佈建]'s end
			[SM: 教育訓練] lasts 7 days
			[SM: 教育訓練] is colored in Coral/LightRed
		@endgantt

.. seealso::

	- "`Creating diagrams in Sphinx`_", 20180908.
		.. _Creating diagrams in Sphinx: https://build-me-the-docs-please.readthedocs.io/en/latest/Using_Sphinx/UsingGraphicsAndDiagramsInSphinx.html
