********************************************************************
About Authors
********************************************************************

.. image:: cph.jpg
   :width: 200px
   :height: 280px
   :scale: 100 %
   :alt: PH Cheng, Ph.D.
   :align: left
   
**Po-Hsun Cheng** received the B.S. degree in information and computer engineering from Chung Yuan Christian University, Chungli, Taiwan, in 1988, 
the M.S. degree in electrical engineering from University Southern California, Los Angeles, California, USA, in 1992, 
and the Ph.D. degree in electronics engineering from National Taiwan University, Taiwan, in 2005. 

He was a teaching assistant (1990-1991) of the Department of Information and Computer Engineering, Chung Yuan Christian University (CYCU), Chungli, Taiwan. 
He was an assistant professor (2008) of the Department of Healthcare Information and Management, Ming Chuan University (MCU), Taipei, Taiwan. 
Since 2008, he has been a member of the faculty in the Department of Software Engineering and Management (SEM), Technology College (TC), National Kaohsiung Normal University, Kaohsiung (NKNU), Taiwan, where he was an Assistant Professor (2008-2010), an Associate Professor (2010-2015), and he is currently a Professor (2015-now).
He is the Dean (2018-2021) of the TC, NKNU.
He is the Chair (2018-2021) of Engineering International Graduate Program, TC, NKNU. 
He was the Chair (2016-2018) of SEM, NKNU. 
Also, he was an adjunct professor (2010-2016) in the Department of Computer Science and Information Engineering, Cheng Shiu University (CSU), Kaohsiung, Taiwan.

He is the Founder and Chair (2017-now) the Information Education Center, NKNU. 
Also, he was the advisory and the convener (2017-2018) of south region, advisory of course and teaching, Information Technology Subject, Technology Field, Central Counseling Group, Ministry of Education (MOE), Taiwan. 
He is the committee member (2017-now), proposition of Advanced Placement Computer Science (APCS), MOE, Taiwan. 
He is the advisory (2018-now) of Teaching Research Center, Technology Field, Middle Education Phase, MOE, Taiwan.

He was the General Chair (2018-2019) of the IEEE CIS Summer School in NKNU, Kaohsiung, Taiwan and its topic was “Computational Intelligence for Human and (Smart) Machine Co-learning”.

Cheng was an honorary fellow (2015) in the Department of Electrical and Computer Engineering, University of Wisconsin, Madison, Wisconsin, USA. 
He was a visiting researcher (2000) in the Department of Computer Science and Engineering, University of California, Riverside, California, USA.

He was a senior product analyst (1993-1994) at Control Data Co. and a system programmer (1994-1995) at AT&T Global Information Solutions Inc. 
He was a programmer (1995-2001), chief (2001-2005) and deputy director (2005-2008) in Information Systems Office, National Taiwan University Hospital (NTUH), Taipei, Taiwan. 
Dr. Cheng leaded a team with 100+ engineers to replace the 26.5-year hospital information systems from IBM mainframe to the N-tier web-based distributed systems (2005-2008).

His research interests include information education, healthcare informatics, assistive technology, software engineering, long-term care systems, design pattern, hardware/software co-design, and parallel programming.

Cheng is a member of the IEEE, the ACM and the IEICE, a senior member of the TAMI, the HL7 Taiwan, and the IICM. 
He joins six IEEE societies, Computer, Computational Intelligence (CIS), Communication, Consumer Electronics (CE), Engineering in Medicine and Biology (EMB), and Systems, Man and Cybernetics (SMC), with membership number, 41416974. 
He is the Chair (2019-2020) of the IEEE SMC Society Tainan Chapter. 
He is the At-large Member (2019-) of the IEEE Computer Society Professional and Educational Activity Board (PEAB).
He is the Executive Director (2018-2019) of the HL7 Taiwan.
He is the Board of Directors (2006-2019) of the TAMI. 
He was the Secretary (2017-2018) of the IEEE SMC Society Tainan Chapter. 
He was the Secretary General (2002-2006) and Board of Directors (2006-2017) of the HL7 Taiwan.

===============================================================================================

**Kuan-Yeh Lin**, Department of Mathematics, College of Science, NKNU. 

===============================================================================================

**Yu-Shan Li**, Department of Mathematics, College of Science, NKNU. 

===============================================================================================

**I-Lin Chou, Ph.D.**, Department of Education, College of Education, NKNU. 

===============================================================================================

**Pei-Gui Tsai, Ph.D.**, Graduate Institute of Interdisciplinary Arts, College of Arts, NKNU. 
