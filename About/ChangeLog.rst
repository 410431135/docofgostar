####################################################################
Change Log
####################################################################

.. note::

	1. Author:	cph
	#. Start:	20080201
	#. Topic:	WhoNotes: Notes of Care Matching
	#. Use:		Sphinx [人面獅身像] and related tools to maintain my study documents
	#. Ref: 	https://archive.org/stream/sphinx-doc/sphinx-doc_djvu.txt
	
.. seealso::

	- `WhoNotes <{pWhoNotesWeb}/index.html>`__

.. todo::

	1. 
	
.. warning::

	1. 
	
********************************************************************
v0.1
********************************************************************
:Date: 20190815

A. New Features
	1. Create this template from Py8.Proj.CareMatching_v0.2.Sphinx	

B. Fixes
	1. Fix logo replacement problem by adding codes at **make.bat**. 
	
		.. code-block:: python
			:linenos:
		
			set STSCI_LOGO_NAME=GoStar4F1
			set STSCI_LOGO_DIR=W:\_D\Exe\Python3632\Lib\site-packages\stsci_rtd_theme\static
			copy /Y %STSCI_LOGO_DIR%\Logo-%STSCI_LOGO_NAME%.png  %STSCI_LOGO_DIR%\stsci_pri_combo_mark_white.png		

		.. note::
		
			- Remember to copy logo file to %STSCI_LOGO_DIR% before make files.
			- Remember to replace logo name at another project.

C. Other Changes
	None. 
		
		
.. seealso::

	- Sphinx Official Web
		http://www.sphinx-doc.org/en/master/index.html
	- Sphinx Documents
		http://www.sphinx-doc.org/en/master/usage/quickstart.html#adding-content
	- Sphinx and RST syntax guide (0.9.3)
		https://thomas-cokelaer.info/tutorials/sphinx/rest_syntax.html
	- Tutorials
		- 20130711: OpenFoundry
			Sphinx - 用 reStructuredText 寫網站與書
				https://www.openfoundry.org/tw/foss-programs/9018-sphinx-restructuredtext-
		- **20160603: Liang-Bo Wang (亮亮)**
			多語系 Sphinx 與 Python 官方文件中文化
				https://blog.liang2.tw/2016Talk-PyDoc-TW/
		- 20180115: 烧煤的快感
			手把手教你Sphinx（一）
				https://blog.csdn.net/gg_18826075157/article/details/79068380
		- 20180122: 烧煤的快感
			手把手教你Sphinx（二）——制作一份学习研讨会报告
				https://blog.csdn.net/gg_18826075157/article/details/79132188
		
