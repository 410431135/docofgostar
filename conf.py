
#*******************************************************************************
# -*- coding: utf-8 -*-
#
# Sphinx RTD theme demo documentation build configuration file, created by
# sphinx-quickstart on Sun Nov  3 11:56:36 2013.
#
# This file is executed with the current directory set to its containing dir.
#
# Note that not all possible configuration values are present in this
# autogenerated file.
#
# All configuration values have a default; values that are commented out
# serve to show the default.
#*******************************************************************************
import sys
import os

################################################################################
################################################################################
#-- Method 1: Enable your name for [make html] environment
#-- Method 2: Or, you can mask off here and set your name for [DeveloperName] in your system environment 
################################################################################
################################################################################
#os.environ["DeveloperName"] = "CPH"
#os.environ["DeveloperName"] = "LKY"	# 林冠曄
#os.environ["DeveloperName"] = "LYS"	# 李昱珊

sys.path.append(os.path.abspath('..'))
sys.path.append(os.path.abspath('exts'))

# from sphinx_rtd_theme import __version__

#===============================================================================
# If extensions (or modules to document with autodoc) are in another directory,
# add these directories to sys.path here. If the directory is relative to the
# documentation root, use os.path.abspath to make it absolute, like shown here.
#===============================================================================
# sys.path.insert(0, os.path.abspath('.'))

#*******************************************************************************
# Extensions
#*******************************************************************************
#===============================================================================
# Ext: sphinxcontrib.plantuml
# Add: CPH @ 20180908
#===============================================================================
if (os.environ["DeveloperName"] == "CPH"):
	os.environ["PlantUML"] = "W:/_D/Lib/Python/PlantUML"
elif (os.environ["DeveloperName"] == "LKY"):
	os.environ["PlantUML"] = "/Users/kylelin/git/F1/docs"
elif (os.environ["DeveloperName"] == "LYS"):
	os.environ["PlantUML"] = "C:/Users/acer/git/GoStar4F1/docs"
elif (os.environ["DeveloperName"] == "UserName"):
	os.environ["PlantUML"] = "/Put/Your/PlantUML/Jar/File/Path/Here"

#-- DO NOT modify this command, please add your path at above lines.
plantuml = 'java -jar ' + os.environ["PlantUML"] + '/plantuml.jar'

#===============================================================================
# Ext: sphinx.ext.mathbase
# Add: CPH @ 20180914
# Ref: https://github.com/sphinx-doc/sphinx/issues/5298
#===============================================================================
math_number_all = True
math_numfig = True
math_eqref_format = "Eq. {number}"
numfig = True
numfig_secnum_depth = 3

#===============================================================================
# Ext: sphinx.ext.mathjax
# Add: CPH @ 20180910
#===============================================================================
#mathjax_path = "https://cdn.mathjax.org/mathjax/latest/MathJax.js?config=TeX-AMS-MML_HTMLorMML"
#mathjax_path = "https://cdnjs.cloudflare.com/ajax/libs/mathjax/2.7.5/latest.js?config=TeX-MML-AM_CHTML"
#mathjax_path = "W:\\_D\\lib\\MathJax\\MathJax-master\\MathJax.js?config=TeX-MML-AM_CHTML"
mathjax_path = "MathJax/MathJax.js?config=TeX-MML-AM_CHTML"
#mathjax_config = {
#    'extensions': ['tex2jax.js'],
#    'jax': ['input/TeX', 'output/HTML-CSS'],
#}

#===============================================================================
# Ext: sphinx.ext.todo
# Add: CPH @ 20180924
# Ref: http://openalea.gforge.inria.fr/doc/openalea/doc/_build/html/source/sphinx/rest_syntax.html?highlight=todo#colored-boxes-note-seealso-todo-and-warnings
#===============================================================================
todo_include_todos=True

#===============================================================================
# Ext: sphinxjp.themes.revealjs
# Add: CPH @ 20180929
# Ref: http://www.xavierdupre.fr/blog/2014-02-01_nojs.html
#===============================================================================
#from pymyinstall import ModuleInstall
#ModuleInstall("sphinxjp.themes.revealjs", "github", "sdpython").install(temp_folder="download")

#===============================================================================
# Ext: sphinxcontrib.slide
# Add: CPH @ 20180929
# Ref: https://github.com/sphinx-contrib/slide
#===============================================================================
#sys.path.append(os.path.abspath('/path/to/sphinxcontrib.slide'))
#sys.path.append(os.path.abspath('C:\\Python3632\\lib\\site-packages\\sphinxcontrib_slide-0.3.0.dist-info'))

#===============================================================================
# Ext: sphinxcontrib.embedly
# Add: CPH @ 201801013
# Ref: https://github.com/jezdez/sphinxcontrib-embedly
#===============================================================================
if (os.environ["DeveloperName"] == "CPH"):
	sys.path.append(os.path.abspath('W:/_D/Exe/Python3632/Lib/site-packages/sphinxcontrib'))
elif (os.environ["DeveloperName"] == "LKY"):
	sys.path.append(os.path.abspath('/anaconda3/envs/sphinx/lib/python3.6/site-packages/sphinxcontrib'))
elif (os.environ["DeveloperName"] == "LYS"):
	sys.path.append(os.path.abspath('/Put/Your/Python/Lib/site-packages/sphinxcontrib'))
elif (os.environ["DeveloperName"] == "UserName"):
	sys.path.append(os.path.abspath('/Put/Your/Python/Lib/site-packages/sphinxcontrib'))

#-------------------------------------------------------------------------------
# Configure the 'Docutils' 
#-------------------------------------------------------------------------------
from sphinxcontrib.embedly import setup_docutils
setup_docutils()
		
#-------------------------------------------------------------------------------
# Configure the 'embedly_key'
#-------------------------------------------------------------------------------
#embedly_key = '<api-key-copied-from-your-account-page>'
embedly_key = '<api-key-copied-from-your-account-page>'

#-------------------------------------------------------------------------------
# Configure the 'embedly_timeout'
#-------------------------------------------------------------------------------
embedly_timeout = 120

#===============================================================================
# Ext: sphinxcontrib.googlemaps
# Add: CPH @ 20190525
# Ref: https://bitbucket.org/birkenfeld/sphinx-contrib/src/default/googlemaps/
#===============================================================================
if (os.environ["DeveloperName"] == "CPH"):
	sys.path.append(os.path.abspath('W:/_D/Exe/Python3632/Lib/site-packages/sphinxcontrib'))
elif (os.environ["DeveloperName"] == "LKY"):
#	sys.path.append(os.path.abspath('/anaconda3/envs/sphinx/lib/python3.6\\lib\\site-packages\\sphinxcontrib'))
	pass
elif (os.environ["DeveloperName"] == "LYS"):
	sys.path.append(os.path.abspath('/Put/Your/Python/Lib/site-packages/sphinxcontrib'))
elif (os.environ["DeveloperName"] == "UserName"):
	sys.path.append(os.path.abspath('/Put/Your/Python/Lib/site-packages/sphinxcontrib'))

#===============================================================================
# Ext: sphinxcontrib.jsdemo
# Add: CPH @ 20190525
# Ref: https://bitbucket.org/prometheus/sphinxcontrib-jsdemo/src/default/
#===============================================================================
if (os.environ["DeveloperName"] == "CPH"):
	sys.path.append(os.path.abspath('W:/_D/Exe/Python3632/Lib/site-packages/sphinxcontrib'))
elif (os.environ["DeveloperName"] == "LKY"):
#	sys.path.append(os.path.abspath('/anaconda3/envs/sphinx/lib/python3.6\\lib\\site-packages\\sphinxcontrib'))
	pass
elif (os.environ["DeveloperName"] == "LYS"):
	sys.path.append(os.path.abspath('/Put/Your/Python/Lib/site-packages/sphinxcontrib'))
elif (os.environ["DeveloperName"] == "UserName"):
	sys.path.append(os.path.abspath('/Put/Your/Python/Lib/site-packages/sphinxcontrib'))

#************************************************************************************
# General Configuration
#************************************************************************************
#===============================================================================
# If your documentation needs a minimal Sphinx version, state it here.
#===============================================================================
needs_sphinx = '1.8'

#===============================================================================
# Add any Sphinx extension module names here, as strings. They can be
# extensions coming with Sphinx (named 'sphinx.ext.*') or your custom ones.
#===============================================================================
extensions = [
    'sphinx.ext.intersphinx',
    'sphinx.ext.autodoc',
	'sphinx.ext.autosectionlabel',
    'sphinx.ext.todo',
    'sphinx.ext.coverage',
#	'sphinx.ext.mathbase',
    'sphinx.ext.mathjax',
#    'sphinx.ext.jsmath',
    'sphinx.ext.ifconfig',
    'sphinx.ext.viewcode',
    'sphinx.ext.graphviz',
    'sphinx.ext.inheritance_diagram',
    'matplotlib.sphinxext.plot_directive',
	# 'hidden_code_block',
	'sphinx_autorun',
#	'sphinxjp.themecore',
#	'sphinx-execute-code',
#    'hieroglyph',
#    'sphinxcontrib.embedly',
	'sphinxcontrib.bibtex',
	'sphinxcontrib.plantuml',
    'sphinxcontrib.httpdomain',
    # 'sphinxcontrib.googlemaps',
	'sphinxcontrib.jsdemo',
#	'sphinxcontrib.googlechart',
#   'sphinxcontrib.fulltoc',
#	'sphinxcontrib.slide',
#   'sphinxcontrib_youtube',
]

graphviz_output_format = 'svg'

#===============================================================================
# Do not warn about external images (status badges in README.rst)
#===============================================================================
suppress_warnings = ['image.nonlocal_uri']

#===============================================================================
# Add any paths that contain templates here, relative to this directory.
#===============================================================================
templates_path = ['_templates']

#===============================================================================
# The suffix of source filenames.
#===============================================================================
source_suffix = '.rst'

#===============================================================================
# The encoding of source files.
#===============================================================================
#source_encoding = 'utf-8-sig'

#===============================================================================
# The master toctree document.
#===============================================================================
master_doc = 'index'

#===============================================================================
# General information about the project.
#===============================================================================
project = u'CareMatching'
copyright = u'Po-Hsun Cheng'

#===============================================================================
# The version info for the project you're documenting, acts as replacement for
# |version| and |release|, also used in various other places throughout the
# built documents.
#
# The short X.Y version.
#===============================================================================
#version = __version__
version = "v0.1.20190815"
# The full version, including alpha/beta/rc tags.
#release = __version__
release = "20190815"

#===============================================================================
# The language for content auto-generated by Sphinx. Refer to documentation
# for a list of supported languages.
#===============================================================================
language = 'zh'

#===============================================================================
# There are two options for replacing |today|: either, you set today to some
# non-false value, then it is used:
#===============================================================================
#today = ''

#===============================================================================
# Else, today_fmt is used as the format for a strftime call.
#===============================================================================
#today_fmt = '%B %d, %Y'

#===============================================================================
# List of patterns, relative to source directory, that match files and
# directories to ignore when looking for source files.
#===============================================================================
exclude_patterns = [
]

#===============================================================================
# The reST default role (used for this markup: `text`) to use for all documents.
#===============================================================================
#default_role = None

#===============================================================================
# If true, '()' will be appended to :func: etc. cross-reference text.
#===============================================================================
#add_function_parentheses = True

#===============================================================================
# If true, the current module name will be prepended to all description
# unit titles (such as .. function::).
#===============================================================================
#add_module_names = True

#===============================================================================
# If true, sectionauthor and moduleauthor directives will be shown in the
# output. They are ignored by default.
#===============================================================================
#show_authors = False

#===============================================================================
# The name of the Pygments (syntax highlighting) style to use.
#===============================================================================
pygments_style = 'default'

#--------
# Is used for 'sphinxjp.themecore'.
#--------
#pygments_style = 'monokai'

#===============================================================================
# A list of ignored prefixes for module index sorting.
#===============================================================================
#modindex_common_prefix = []

#===============================================================================
# Intersphinx Mapping
#===============================================================================
intersphinx_mapping = {'rtd': ('https://docs.readthedocs.io/en/latest/', None)}


#*******************************************************************************
# Options for HTML output 
#*******************************************************************************

'''
#===============================================================================
Theme: sphinx_rtd_theme
# Note: was replaced by stsct_rtd_theme @ 20181013, CPH.
#===============================================================================
#-------------------------------------------------------------------------------
# The theme to use for HTML and HTML Help pages.
# See the documentation for a list of builtin themes.
#-------------------------------------------------------------------------------
# html_theme = 'sphinx_rtd_theme'

#--------
# Is used for 'sphinxjp.themecore'.
#--------
#html_theme = 'htmlslide'

#-------------------------------------------------------------------------------
# Theme options are theme-specific and customize the look and feel of a theme further.
# For a list of options available for each theme, see the documentation.
#-------------------------------------------------------------------------------
html_theme_options = {
    'logo_only': True
}

#-------------------------------------------------------------------------------
# Add any paths that contain custom themes here, relative to this directory.
#-------------------------------------------------------------------------------
html_theme_path = ["../.."]

#-------------------------------------------------------------------------------
# The name for this set of Sphinx documents. 
# If None, it defaults to "<project> v<release> documentation".
#-------------------------------------------------------------------------------
#html_title = None

#-------------------------------------------------------------------------------
# A shorter title for the navigation bar.
# Default is the same as html_title.
#-------------------------------------------------------------------------------
#html_short_title = None

#-------------------------------------------------------------------------------
# The name of an image file (relative to this directory) to place at the top of the sidebar.
#-------------------------------------------------------------------------------
html_logo = "Logo-CareMatching.png"

#-------------------------------------------------------------------------------
# The name of an image file (within the static path) to use as favicon of the docs.
# This file should be a Windows icon file (.ico) being 16x16 or 32x32 pixels large.
#-------------------------------------------------------------------------------
#html_favicon = None

#-------------------------------------------------------------------------------
# Add any paths that contain custom static files (such as style sheets) here,
# relative to this directory. They are copied after the builtin static files,
# so a file named "default.css" will overwrite the builtin "default.css".
#-------------------------------------------------------------------------------
#html_static_path = ['_static']

#-------------------------------------------------------------------------------
# If not '', a 'Last updated on:' timestamp is inserted at every page bottom,
# using the given strftime format.
#-------------------------------------------------------------------------------
html_last_updated_fmt = '%b %d, %Y'

#-------------------------------------------------------------------------------
# If true, SmartyPants will be used to convert quotes and dashes to typographically correct entities.
#-------------------------------------------------------------------------------
#html_use_smartypants = True

#-------------------------------------------------------------------------------
# Custom sidebar templates, maps document names to template names.
#-------------------------------------------------------------------------------
#html_sidebars = {}

#-------------------------------------------------------------------------------
# Additional templates that should be rendered to pages, maps page names to template names.
#-------------------------------------------------------------------------------
#html_additional_pages = {}

#-------------------------------------------------------------------------------
# If false, no module index is generated.
#-------------------------------------------------------------------------------
#html_domain_indices = True

#-------------------------------------------------------------------------------
# If false, no index is generated.
#-------------------------------------------------------------------------------
html_use_index = True

#-------------------------------------------------------------------------------
# If true, the index is split into individual pages for each letter.
#-------------------------------------------------------------------------------
#html_split_index = False

#-------------------------------------------------------------------------------
# If true, links to the reST sources are added to the pages.
#-------------------------------------------------------------------------------
html_show_sourcelink = True

#-------------------------------------------------------------------------------
# If true, "Created using Sphinx" is shown in the HTML footer. Default is True.
#-------------------------------------------------------------------------------
html_show_sphinx = True

#-------------------------------------------------------------------------------
# If true, "(C) Copyright ..." is shown in the HTML footer. Default is True.
#-------------------------------------------------------------------------------
html_show_copyright = True

#-------------------------------------------------------------------------------
# If true, an OpenSearch description file will be output, and all pages will
# contain a <link> tag referring to it.  The value of this option must be the
# base URL from which the finished HTML is served.
#-------------------------------------------------------------------------------
#html_use_opensearch = ''

#-------------------------------------------------------------------------------
# This is the file name suffix for HTML files (e.g. ".xhtml").
#-------------------------------------------------------------------------------
#html_file_suffix = None

#-------------------------------------------------------------------------------
# Output file base name for HTML help builder.
#-------------------------------------------------------------------------------
htmlhelp_basename = 'GoStar4F1Doc'

#===============================================================================
End of sphinx_rtd_theme
#===============================================================================
'''


#===============================================================================
# Theme: stsci_rtd_theme
# Note: Replace the sphinx_rtd_theme @ 20181013, CPH
#===============================================================================
#-------------------------------------------------------------------------------
# Making this theme work on Readthedocs
#-------------------------------------------------------------------------------
import sphinx
import stsci_rtd_theme

#####======#####======#####======#####======#####======#####======#####======#####======
##### ALSO, NEED to def setup() and the code is put at the end of this conf.py file.
#####======#####======#####======#####======#####======#####======#####======#####======
#def setup(app):
#    app.add_stylesheet("stsci.css")
   
    
#...............................................................................
# the below is not strictly necessary but helps with extensions you may use across versions
#...............................................................................
from distutils.version import LooseVersion

on_rtd = os.environ.get('READTHEDOCS', None) == 'True'
if on_rtd:
    extensions.append('sphinx.ext.mathjax')
elif LooseVersion(sphinx.__version__) < LooseVersion('1.4'):
    extensions.append('sphinx.ext.pngmath')
else:
    extensions.append('sphinx.ext.imgmath')    

#-------------------------------------------------------------------------------
# The theme to use for HTML and HTML Help pages.
# See the documentation for a list of builtin themes.
#-------------------------------------------------------------------------------
html_theme = 'stsci_rtd_theme'

#-------------------------------------------------------------------------------
# Theme options are theme-specific and customize the look and feel of a theme further.
# For a list of options available for each theme, see the documentation.
#-------------------------------------------------------------------------------
html_theme_options = {
    'logo_only': True
}

#-------------------------------------------------------------------------------
# Add any paths that contain custom themes here, relative to this directory.
#-------------------------------------------------------------------------------
html_theme_path = [stsci_rtd_theme.get_html_theme_path()]

#-------------------------------------------------------------------------------
# The name for this set of Sphinx documents. 
# If None, it defaults to "<project> v<release> documentation".
#-------------------------------------------------------------------------------
#html_title = None
html_title = 'GoStar4F1@Apacph'

#-------------------------------------------------------------------------------
# A shorter title for the navigation bar.
# Default is the same as html_title.
#-------------------------------------------------------------------------------
#html_short_title = None
html_short_title = 'GoStar4F1@Apacph'

#-------------------------------------------------------------------------------
# The name of an image file (relative to this directory) to place at the top of the sidebar.
#-------------------------------------------------------------------------------
html_logo = "Logo-GoStar4F1.png"

#-------------------------------------------------------------------------------
# The name of an image file (within the static path) to use as favicon of the docs.
# This file should be a Windows icon file (.ico) being 16x16 or 32x32 pixels large.
#-------------------------------------------------------------------------------
#html_favicon = None

#-------------------------------------------------------------------------------
# Add any paths that contain custom static files (such as style sheets) here,
# relative to this directory. They are copied after the builtin static files,
# so a file named "default.css" will overwrite the builtin "default.css".
#-------------------------------------------------------------------------------
#html_static_path = ['_static']
html_static_path = ['_static', '_assets']

#-------------------------------------------------------------------------------
# If not '', a 'Last updated on:' timestamp is inserted at every page bottom,
# using the given strftime format.
#-------------------------------------------------------------------------------
html_last_updated_fmt = '%b %d, %Y'

#-------------------------------------------------------------------------------
# If true, SmartyPants will be used to convert quotes and dashes to typographically correct entities.
#-------------------------------------------------------------------------------
#html_use_smartypants = True

#-------------------------------------------------------------------------------
# Custom sidebar templates, maps document names to template names.
#-------------------------------------------------------------------------------
#html_sidebars = {}

#-------------------------------------------------------------------------------
# Additional templates that should be rendered to pages, maps page names to template names.
#-------------------------------------------------------------------------------
#html_additional_pages = {}

#-------------------------------------------------------------------------------
# If false, no module index is generated.
#-------------------------------------------------------------------------------
#html_domain_indices = True

#-------------------------------------------------------------------------------
# If false, no index is generated.
#-------------------------------------------------------------------------------
html_use_index = True

#-------------------------------------------------------------------------------
# If true, the index is split into individual pages for each letter.
#-------------------------------------------------------------------------------
#html_split_index = False

#-------------------------------------------------------------------------------
# If true, links to the reST sources are added to the pages.
#-------------------------------------------------------------------------------
html_show_sourcelink = True

#-------------------------------------------------------------------------------
# If true, "Created using Sphinx" is shown in the HTML footer. Default is True.
#-------------------------------------------------------------------------------
html_show_sphinx = True

#-------------------------------------------------------------------------------
# If true, "(C) Copyright ..." is shown in the HTML footer. Default is True.
#-------------------------------------------------------------------------------
html_show_copyright = True

#-------------------------------------------------------------------------------
# If true, an OpenSearch description file will be output, and all pages will
# contain a <link> tag referring to it.  The value of this option must be the
# base URL from which the finished HTML is served.
#-------------------------------------------------------------------------------
#html_use_opensearch = ''

#-------------------------------------------------------------------------------
# This is the file name suffix for HTML files (e.g. ".xhtml").
#-------------------------------------------------------------------------------
#html_file_suffix = None

#-------------------------------------------------------------------------------
# Output file base name for HTML help builder.
#-------------------------------------------------------------------------------
htmlhelp_basename = 'GoStar4F1Doc'

#===============================================================================
# End of stsci_rtd_theme
#===============================================================================


#*******************************************************************************
# Options for LaTeX output 
#*******************************************************************************
#===============================================================================
latex_elements = {
# The paper size ('letterpaper' or 'a4paper').
#'papersize': 'letterpaper',

# The font size ('10pt', '11pt' or '12pt').
#'pointsize': '10pt',

# Additional stuff for the LaTeX preamble.
#'preamble': '',
}

#===============================================================================
# Grouping the document tree into LaTeX files. List of tuples
# (source start file, target name, title, author, documentclass [howto/manual]).
#===============================================================================
latex_documents = [
  ('index', 'GoStar4F1.tex', u'GoStar4F1 Documentation',
   u'Po-Hsun Cheng', 'manual'),
]

#===============================================================================
# The name of an image file (relative to this directory) to place at the top of
# the title page.
#===============================================================================
#latex_logo = None

#===============================================================================
# For "manual" documents, if this is true, then toplevel headings are parts, not chapters.
#===============================================================================
#latex_use_parts = False

#===============================================================================
# If true, show page references after internal links.
#===============================================================================
#latex_show_pagerefs = False

#===============================================================================
# If true, show URL addresses after external links.
#===============================================================================
#latex_show_urls = False

#===============================================================================
# Documents to append as an appendix to all manuals.
#===============================================================================
#latex_appendices = []

#===============================================================================
# If false, no module index is generated.
#===============================================================================
#latex_domain_indices = True


# -- Options for manual page output --------------------------------------------

#===============================================================================
# One entry per manual page. List of tuples
# (source start file, name, description, authors, manual section).
man_pages = [
    ('index', 'GoStar4F1', u'GoStar4F1 Documentation',
     [u'Po-Hsun Cheng'], 1)
]

# If true, show URL addresses after external links.
#man_show_urls = False


#*******************************************************************************
# Options for Texinfo output
#*******************************************************************************
#===============================================================================
# Grouping the document tree into Texinfo files. 
# List of tuples
# (source start file, target name, title, author, dir menu entry, description, category)
#===============================================================================
texinfo_documents = [
  ('index', 'GoStar4F1', u'GoStar4F1 Documentation',
   u'Po-Hsun Cheng', 'GoStar4F1',
   'GoStar4F1 for Po-Hsun Cheng, Ph.D.', 'Miscellaneous'),
]

#===============================================================================
# Documents to append as an appendix to all manuals.
#===============================================================================
#texinfo_appendices = []

#===============================================================================
# If false, no module index is generated.
#===============================================================================
#texinfo_domain_indices = True

#===============================================================================
# How to display URL addresses: 'footnote', 'no', or 'inline'.
#===============================================================================
#texinfo_show_urls = 'footnote'


#*******************************************************************************
# Options for sphinx-intl
#*******************************************************************************
locale_dirs = ["locale/"]
gettext_compact = False


#*******************************************************************************
# Function:	Global Variables Definition Area
# Author:	CPH
# Start:	20190207
#*******************************************************************************
def ultimateReplace(app, docname, source):
    result = source[0]
    for key in app.config.ultimate_replacements:
        result = result.replace(key, app.config.ultimate_replacements[key])
    source[0] = result

ultimate_replacements = {
	"{pPy3Root}"		: "W:/_D/Data.cph/Workspace/Python3",
	"{pGoStar4F1Git}"	: "W:/_D/Data.cph/Workspace/Python3/Py8.Proj.GoStar4.F1.Sphinx.Git/docs/build/html",
	"{pWhoNotesWeb}"	: "W:/_D/Data.cph/Workspace/Python3/Py8.Proj.WhoNotes_v1.11.Sphinx/docs/build/html",
    "{pProceeding}"		: "W:/E.Proceedings",
    "{pSpeech}"			: "W:/W.Speech",
}

def setup(app):
   app.add_config_value('ultimate_replacements', {}, True)
   app.connect('source-read', ultimateReplace)

   #===============================================================================
   # Theme: stsci_rtd_theme
   # Note: Replace the sphinx_rtd_theme @ 20181013, CPH
   #       Also, part of definitions are defined at the above part of this conf.py file. 
   #===============================================================================
   app.add_stylesheet("stsci.css")

   #===============================================================================
   # Function: Online Run Python Codes and Show Output on Screen Below the Codes
   # Note: Replace @ 20190518, CPH
   # Test: W:\_D\Data.cph\Workspace\JavaScript\p9001OnlineRunPyCode_RecursiveTurtle
   # Ref.: https://artofproblemsolving.com/assets/pythonbook/recursion.html
   #===============================================================================
   app.add_javascript('RunPyCode/jquery.js')
   app.add_javascript('RunPyCode/underscore.js')
   app.add_javascript('RunPyCode/doctools.js')
   app.add_javascript('RunPyCode/pywindowCodemirrorC.js')
   app.add_javascript('RunPyCode/skulpt.min.js')
   app.add_javascript('RunPyCode/skulpt-stdlib.js')
   app.add_javascript('RunPyCode/aopsmods.js')
   
#   app.add_stylesheet("RunPyCode/basic.css")
#   app.add_stylesheet("RunPyCode/style.css")
#   app.add_stylesheet("RunPyCode/pygments.css")
#   app.add_stylesheet("RunPyCode/codemirrorEdited.css")
   
