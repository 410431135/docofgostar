.. note::
	- Start: 20171209
	- System Environment:
		- Python v3.6.3
		- plantuml.jar v1.2018.10
		- graphviz v0.9 (v2.38)
		
####################################################################
Sequence Diagram
####################################################################

********************************************************************
Change Log
********************************************************************
A. v0.1-20190912
#. v0.2-20190921 add 瀏覽、檢舉文章
#. v0.3-20190922 add 履歷管理系統
#. v0.4-20190928 revise several diagrams after Friday Meeting


********************************************************************
Diagrams
********************************************************************

====================================================================
v0.4-20191001 reivse 履歷, add 文章
====================================================================


--------------------------------------------------------------------
身份
--------------------------------------------------------------------

使用者新增身份
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. uml::
	
	@startuml
	actor User order 1
	participant ":履歷管理頁面" as page order 2
	database Data as db order 100

	activate page
	User -> page : 點選新增身份按鈕\n newIdentificationBtn
	create ":新增身份畫面" as id order 3
	page -> id : newIdentification(User.sAccount)
	activate id
	User <-- id : show 新增身份畫面
	User -> id : 選擇欲新增的身份
	note right
		7+1種身份
	end note
	opt 選擇身份年限
		User -> id : 選擇身份年限
	end
	User -> id : 上傳身份佐證
	id -> db : uploadFile()
	User -> id : 點擊送出審查\n click checkBtn
	deactivate id

	ref over id, db : 管理員審查身份
	...  awaiting 管理員審查 ...

	alt 審查通過
		page <- db : getStatus()
		note right
			iStatus == 1 represent that
			the User got the identification,
			or == 0 if the request is in process,
			or < 0 if the request is objected.
		end note
		User <- page : 顯示該身份通過審核
	else 審查不通過
		page <- db : getStatus()
		User <- page : 顯示該身份不通過審核
		opt 查看不通過原因
			User -> page : 點擊不通過原因\n showCheckResult()
			return : 不通過原因\n sCheckResult
		end
	else 審查中
		page <- db : [while user refresh the page.]\ngetStatus()
		User <- page : 顯示該身份審核中
		opt 重送審核
			User -> page : 點擊重送審核\n resendRequest()
			ref over page, db : 管理員審查身份
		end
	end
	@enduml

ref-管理員審查身份
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. uml::
	
	@startuml
	title ref 管理員審查身份
	actor Manager order 1
	participant ":履歷審查頁面" as page order 2
	database Data as db order 100

	activate page
	page <- db : [while 有新的送審要求]\n新的審查資料
	Manager -> page : 查閱待審資料
	alt 審查屬實
		Manager -> page : 點擊通過審查
	else 審查有疑慮（不通過）
		Manager -> page : 填寫不通過原因\n setCheckResult()
		Manager -> page : [if sCheckResult is filled]\n點擊不通過審查
	end
	page -> db : setStatus()
	@enduml


--------------------------------------------------------------------
文章
--------------------------------------------------------------------

瀏覽文章
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. uml::
	
	@startuml
	actor User order 1
	participant ":瀏覽文章頁面" as page order 2
	database Data as db order 100

	User -> page : click 某篇文章
	activate page
	page -> db : get article page
	activate db
	page <-- db : direct to ArticlePage with iSerialNumberOfArticle
	deactivate db
	User <- page : show article

	@enduml

.. note::
	
	#. how to write if i want to show
	#. that this is to direct to the page i want

瀏覽文章-於文章留言
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. uml::
	
	@startuml
	actor User order 1
	participant ":瀏覽文章頁面" as page order 2
	database Data as db order 100

	activate page

	User -> page : 撰寫留言內容
	User -> page : click 留言\n click commentBtn
	create ":留言" as comment order 3
	page -> comment : sendComment(sContentOfComment) 
	activate comment
	comment -> db : setComment(User.sName, sContentOfComment, date)
	db --> page : refresh page
	User <- page : able to see the comment he/she just left.

	@enduml

瀏覽文章-於文章按讚
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. uml::
	
	@startuml
	actor User order 1
	participant ":瀏覽文章頁面" as page order 2
	database Data as db order 100

	activate page

	User -> page : click 讚\n click likeBtn
	alt [if 該用戶尚未對此文章按讚過]
		page -> db : setWhoLikes(User.sAccount)
	else [該用戶已經按讚過]
		page -> db : setWhoLikes(User.sAccount)
		note right
		if the user has liked the article
		he/she would be removed from sWhoLikes/
		vice versa
		end note
	end
	activate db
	db -> db : setNumberOfLikes()
	activate db
	page <-- db : getLikeStatus(User.sAccount)
	deactivate db
	@enduml

瀏覽文章-收藏文章
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. uml::
	
	@startuml
	actor User order 1
	participant ":瀏覽文章頁面" as page order 2
	database Data as db order 100

	activate page

	User -> page : click 收藏\n click collectBtn
	page -> db : setWhoCollect(User.sAccount)
	activate db
	db -> db : sWhoCollect += User.sAccount
	activate db
	page <-- db : getCollectStatus(User.sAccount)
	deactivate db
	@enduml

瀏覽文章-檢舉文章

.. uml::
	
	@startuml
	actor User order 1
	participant ":瀏覽文章頁面" as page order 2
	database Data as db order 100

	activate page

	User -> page : click 檢舉\n click reportBtn
	create ":檢舉畫面" order 3
	page -> ":檢舉畫面" : createReportPage(iSerialNumberOfArticle, User.sAccount)
	User <-- ":檢舉畫面" : show 檢舉畫面\nnew Report
	User -> ":檢舉畫面" : 選擇並填寫原因
	User -> ":檢舉畫面" : 送出檢舉\n click sendReportBtn
	":檢舉畫面" -> db : setReportRecord(iSerialNumberOfArticle, iClassification)
	page <- db : show 檢舉送出結果
	@enduml

--------------------------------------------------------------------
檢舉
--------------------------------------------------------------------

審查檢舉文章
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. uml::
	
	@startuml
	actor Manager order 1
	database Data as db order 100
	participant ":檢舉審查頁面"  as page order 2
	actor 被檢舉人 as qq order 101
	actor 檢舉人 as teller order 102

	activate db
	db -> page : [when update page]\n 顯示有新的檢舉
	activate page
	deactivate db

	Manager -> page : 查看待審查的檢舉
	alt 檢舉屬實
		Manager -> page : 填寫檢舉審查結果\n sCheckResult
		Manager -> page : click reportAcceptBtn
		create ":選擇懲處" as punish order 4
		page -> punish : void createPunishPage()
		Manager -> punish : 選擇處分\n sPunishment
		Manager -> punish : click punishBtn
		punish -> db : punish()

	else 檢舉駁回
		Manager -> page : 填寫檢舉審查結果\n sCheckResult
		Manager -> page : reportRejectBtn
		page -> db : reportReject()
		
	end
	db -> db : setReportStatus()
	activate db
	page <- db : refresh the page
	deactivate db

	par
		db -> qq : inform(User.sAccount of 檢舉人, User.sAccount of 被檢舉人)
		db -> teller : inform(User.sAccount of 檢舉人, User.sAccount of 被檢舉人)
	end

	@enduml

.. note::

	#. 提交複查，尚未確定如何做


審查檢舉文章-被檢舉人提交複查
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. uml::
	
	@startuml
	actor 被檢舉人 as qq order 1
	actor Manager order 3
	participant ":通知頁面"  as inform order 2
	participant ":檢舉審查頁面"  as Manpage order 4
	database Data as db order 100

	activate qqpage
	inform -> qq : 通知檢舉結果\n inform(User.sAccount)
	qq -> qqpage : click 提交複查\n reportObjectBtn
	qq -> qqpage : 填寫複查原因
	qq -> qqpage : 送出\n reportObject(sReportObjection)
	qqpage -> db : 新增檢舉複查\n new Report
	note right
	Report iStatus == 2 表示複查
	end note
	activate db
	db -> Manpage : 更新複查案
	deactivate db
	activate Manpage
	Manpage -> Manager : 通知複查
	deactivate Manpage
	alt 變更處分
		Manager -> Manpage : 變更處分
		activate Manpage
	else 維持處分
		Manager -> Manpage : 維持處分
	end
	Manpage -> db : update 處分
	activate db
	db -> qq : inform 複查結果
	deactivate db


	@enduml


發表文章-進入發表文章
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. uml::

	@startuml
	actor User order 1
	database Database as Data order 100
	participant ":發表文章按鈕" as btn order 2
	participant ":文章編輯頁面" as page order 3

	title 發表文章-進入發表文章
	== Initialization ==
	User -> btn : click editArticleBtn
	activate btn
	create page
	btn ->  page : createEditArticlePage(sBoardName)
	activate page
	deactivate btn
	page -> Data : retriveEditngArtical(User.sAccount)
	activate Data
	return iSerialNumberOfArticle
	deactivate Data
	User <-- page : direct to artical editing page
	User -> page : 編輯內文
	@enduml


發表文章-編輯時
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. uml::

	@startuml
	actor User order 1
	database Database as Data order 100
	'participant ":發表文章按鈕" as btn order 2
	participant ":文章編輯頁面" as page order 3
	participant ":發文規則按鈕" order 4
	participant ":預覽文章按鈕" order 6
	
	participant ":儲存按鈕" order 90

	title 編輯時
	== Editing Artical ==
	activate page
	group 設定一般文章設定
	opt 查看發文規則
		User -> ":發文規則按鈕" : click articleRuleBtn
		activate ":發文規則按鈕" 
		create ":發文規則頁面" order 5
		":發文規則按鈕" -> ":發文規則頁面" : createArticleRulePage(sBoardName)
		deactivate ":發文規則按鈕"
		activate ":發文規則頁面"
		":發文規則頁面" -> Data : getArticleRule(sBoardName)
		activate Data
		":發文規則頁面" <-- Data : sArticleRule
		deactivate Data
		User <-- ":發文規則頁面" : Rule page
		User -> ":發文規則頁面" : close rule page
		destroy ":發文規則頁面"
	else 預覽文章
		User -> ":預覽文章按鈕" : click previewBtn
		activate ":預覽文章按鈕"
		create ":預覽文章頁面" order 7
		par
			":預覽文章按鈕" -> ":預覽文章頁面" : createPreviewPage()
			activate ":預覽文章頁面"
			":預覽文章按鈕" -> Data : saveArticle(iSerialNumberOfArticle)
		end
		deactivate ":預覽文章按鈕"	
		":預覽文章頁面" -> Data : retriveEditngArtical(USer.sAccount)
		activate Data
		return iSerialNumberOfArticle
		deactivate Data
		User <-- ":預覽文章頁面" : show 預覽文章頁面
		User -> ":預覽文章頁面" : close()
		destroy ":預覽文章頁面"
	end
	@enduml

發表文章-儲存及發表
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. uml::

	@startuml
	actor User order 1
	database Database as Data order 100
	'participant ":發表文章按鈕" as btn order 2
	participant ":文章編輯頁面" as page order 3
	'participant ":發文規則按鈕" order 4
	'participant ":預覽文章按鈕" order 6
	
	participant ":儲存按鈕" order 90

	title 儲存及發表
	== Saving/Publishing Artical ==
	activate page
	opt 儲存文章
	User -> ":儲存按鈕" : click saveArticlebtn
	activate ":儲存按鈕"
	":儲存按鈕" -> Data : saveArticle(iSerialNumberOfArticle, sContent, )
	note right
		saving all changes including:
		內文、板塊、標題、身份、閱讀權限
	end note
	activate Data
	return <<Response>>
	deactivate Data
	User <-- ":儲存按鈕" : <<SaveSuccessfulMessage>>
	deactivate ":儲存按鈕"
	end

	User -> page : click nextBtn
	page -> page : isAllColumnFilled()
	activate page


	create ":編輯話題頁面" as tag order 21
	page -> tag : [if True] \ncreateArticleTopicsPage()
	deactivate page
	activate tag
	User <-- tag : show 編輯話題頁面\n ArticlePage
	'deactivate next
	User -> tag : 選擇或輸入話題標籤並按發布\n click publishBtn
	tag -> Data : saveArticle(iSerialNumberOfArticle, sContent, )
	activate Data
	tag <-- Data : <<Response>>
	create ":文章頁面" as artical order 30
	tag -> artical : [if saved successfully]\npublish(iSerialNumberOfArticle)
	activate artical
	deactivate tag
	deactivate page
	artical -> Data : retriveArtical(iSerialNumberOfArticle)
	artical <-- Data : Article
	deactivate Data
	User <-- artical : direct to artical page

	@enduml

--------------------------------------------------------------------
審查特定履歷
--------------------------------------------------------------------

.. uml::
	
	@startuml
	actor User order 1
	participant ":總履歷介面" as page order 2
	database Data as db order 100

	activate page
	User -> page : [if specificCV.iStatus < 0]\n勾選欲審查的項目
	User -> page : 點擊送出審查\n click submitBtn
	page -> db : 將項目送入待審查\n submit(iSerialNumber)
	activate db
	db -> page : set 項目狀態 = 待審查\n specificCV.iStatus = -2
	note right
	iStatus =
	1:pass
	-1:not been submitted
	-2: submitted but not pass yet
	-3: submitted and been rejected
	end note
	deactivate db

	ref over db : 管理員審查
	... awaiting 管理員審查 ...

	alt 審查通過
		db -> db : set 項目狀態 = 已認證\n specificCV.iStatus = 1
		activate db
		db -> page : [if User refresh the page ]\n更新項目狀態\ngetiStatus(iSerialNumber)
		deactivate db
	else 審查不通過
		db -> db : set 項目狀態 = 不通過\n specificCV.iStatus = -3
		activate db
		db -> page : [if User refresh the page ]\n更新項目狀態\ngetiStatus(iSerialNumber)
		deactivate db
	end
		page -> User : 通知審查結果\n informUserResult(iSerialNumber, iStatus)

	@enduml

reference-管理員審查

.. uml::

	@startuml
	actor Manager as m order 1
	participant ":審查履歷介面" as page order 2
	database Data as db order 100

	m -> page : click updateExamineeCVBtn
	page -> db : 更新審查要求通知\nupdateExamineeCV()
	activate db
	activate page
	db --> page : render data
	deactivate db
	m -> page : 點擊未審查項目\n order by 同一個人 or 同一個項目
	alt 予以通過項目
		m -> page : 勾選通過項目
		m -> page : 點擊通過按鈕\n click passCVBtn
		page -> db : set 項目狀態 = 已認證\n passCV(SpeicalCV.iSerialNumber)
		activate db
		page -> page : refresh page
		deactivate db
	else 不予以通過項目
		m -> page : 勾選不通過項目
		m -> page : 填寫不通過原因
		m -> page : 點擊不通過按鈕\n click noPassCVBtn
		page -> db : set 項目狀態 = 不通過\n noPassCV(resaon, SpeicalCV.iSerialNumber)
		page -> page : refresh 頁面
		deactivate db
	end
	@enduml	
--------------------------------------------------------------------
設定履歷
--------------------------------------------------------------------

履歷總管-新增（填寫）履歷

.. uml::
	
	@startuml
	actor User order 1
	participant ":履歷總管" as all order 2
	database Data as db order 100

	User -> all : 進入履歷總管
	activate all
	User -> all : 填寫履歷內容
	User -> all : click 變更(儲存)\n click saveCVBtn
	all -> db : saveCV()
	activate db
	db --> all : return <<success / fail>>
	deactivate db
	all -> User : 顯示變更結果

	@enduml

新增履歷-以及設定隱私

.. uml::
	
	@startuml
	actor User order 1
	participant ":履歷總覽" as A order 2
	participant ":新增履歷頁面" as B order 3
	participant ":特定履歷頁面" as C order 4
	database Data as db order 100

	User -> A : 進入履歷總覽
	activate A
	opt 新增履歷
		User -> A :點擊 \n click newCVBtn
		A -> B : newCV()
		activate B
		B --> User : show page
		User -> B : 填寫履歷名稱
		User -> B :click 儲存\n saveAndCreateCV(sCVName)
		B -> db : create MyCV
		deactivate B
		activate db
	else 選擇欲設定的履歷
		User -> A :選擇欲設定的履歷
	end
	deactivate db
	C -> User : direct to 特定履歷表頁面
	activate C
	User -> C : 設定各項隱私 顯示／隱藏\n set [some title = 1 or 0]
	User -> C : click 儲存\n saveMyCV()
	C -> db : update data

	@enduml

====================================================================
v0.4-20190928 revise several diagrams after Friday Meeting
====================================================================

--------------------------------------------------------------------
活動群組
--------------------------------------------------------------------

設立活動群組
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. uml::
	
	@startuml
	actor User order 1
	participant ":活動群組版塊" as page order 2
	participant ":活動群組資料填寫頁面" as info order 3
	participant ":活動群組討論頁面" as main order 4
	database Data as db order 100

	activate page
	User -> page : 點擊 設立活動群組\n click createGroupBtn
	page -> info : createGroup(User.sName)
	deactivate page
	activate info
	info -> User : show page
	User -> info : 填寫資料
	User -> info : click OK

	loop while bCheckExistSameName(sGroupTitle)
		info -> User : showSameTitleMessage()
	end

	info -> db : [if !bCheckExistSameName(sGroupTitle)]\n store({User.sName, sGroupTitle, })
	activate db
	info -> User : [if get no worng message from db]\n showCreateSucessfully()
	info -> main : createPage(sGroupTitle)
	deactivate info
	activate main
	main -> db : retrive group infomation\n getGroupInfo(sGroupTitle)
	db -> main : render data
	deactivate db
	main -> User : direct to 活動群組討論頁面\n redirect(sGroupTitle)
	@enduml

加入活動成員
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. uml::
	
	@startuml
	actor User order 1
	participant ":活動群組頁面" as page order 2
	participant ":搜尋使用者頁面" as se order 3
	database Data as db order 100

	activate page
	User -> page : click 加入活動成員 \n click addMemberBtn
	page -> se : createSearchPage()
	activate se
	se -> User : show SearchPage
	opt 搜尋人名
		User -> se : 輸入人名
		se -> db : findUsers(User.sName)
		activate db
		db --> se : [(User.Name, User.sEmail), ]
		deactivate db
		User -> se : 選擇要加入的人
		ref over se : 選擇要加入的人
		User -> se : 點擊 送出邀請\n click inviteBtn
		se -> db : invite(User.sName)
		db --> se : invite(User.sName)\n <<Message: fail/success>>
	else 複製連結
		User -> se : click createURLbtn
		se --> User : createURL()
		User -> se : copy URL
	end

	@enduml

ref - 選擇要加入的人

.. uml::
	
	@startuml
	note right
	希望可以顯示頭貼、人名
	並且當游標至於上時，
	可以顯示系級、短簡介
	end note

	@enduml	


加入活動群組
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. uml::
	
	@startuml
	actor User order 1
	participant ":通知頁面" as inform order 2
	participant ":活動群組版塊" as page order 3
	database Data as db order 100

	alt 收到群組邀請
		User -> inform : 點擊接受\n click acceptBtn
		activate inform
		inform -> db : addMember(User.name)
		inform --> User : show message \n已成功加入活動群組
		deactivate inform
	else 點擊群組邀請網址
		User -> inform : 點擊接受\n click acceptBtn
		activate inform
		inform -> db : addMember(User.name)
		inform --> User : show message \n已成功加入活動群組
		deactivate inform
	else 自己申請加入

		User -> page : 搜尋欲加入的群組
		User -> page : [if iStatus > 0]\n click JoinBtn
		page -> User : sJoinGroup(User.sName)\n<<Message : 申請已送出>>
		ref over page, db : 申請加入
		... awaiting ...
		page -> User : inform User 成功加入／被拒絕加入
	end
	@enduml

option-收到群組邀請

.. uml::
	
	@startuml
	actor User order 1
	participant ":通知頁面" as inform order 2
	participant ":活動群組版塊" as page order 3
	database Data as db order 100

	[-> User : informGroupInvite(sGroupTitle, User.sName)
	alt 接受
		User -> inform : 點擊接受\n click acceptBtn
		activate inform
		inform -> db : addMember(User.name)
		inform --> User : show message \n已成功加入活動群組
		deactivate inform
	else 不接受
		User -> inform : 點擊叉叉\n該通知被關閉
		
	end
	@enduml

option-點擊群組邀請網址

.. uml::
	
	@startuml
	actor User order 1
	participant ":通知頁面" as inform order 2
	participant ":活動群組版塊" as page order 3
	database Data as db order 100

	title 點擊群組邀請網址

	[-> User : [when click URL]\n invite(User.sName)
	alt 接受
		User -> inform : 點擊接受\n click acceptBtn
		activate inform
		inform -> db : addMember(User.name)
		inform --> User : show message \n已成功加入活動群組
		deactivate inform
	else 不接受
		User -> inform : 點擊叉叉\n該通知被關閉
		
	end
	@enduml

option-自己申請加入

.. uml::

	@startuml
	actor User order 1
	participant ":通知頁面" as inform order 2
	participant ":活動群組版塊" as page order 3
	database Data as db order 100

	title 自己申請加入

		User -> page : 搜尋欲加入的群組
		User -> page : [if iStatus > 0]\n click JoinBtn
		page -> User : sJoinGroup(User.sName)\n<<Message : 申請已送出>>
		ref over page, db : 申請加入
		... awaiting ...
		page -> User : inform User 成功加入／被拒絕加入\n inform(sMessage, User.sName)
	
	@enduml

ref-申請加入

.. uml::

	@startuml
	actor 群組管理人員 as man order 1
	participant ":通知頁面" as inform order 2
	database Data as db order 100

	activate inform
	inform -> man : 通知要求加入活動群組
	alt 同意
		man -> inform : 點擊同意\n click acceptBtn
		inform -> db : addMember(User.sName)
		activate db
		db --> inform : <<Message: success>>
		deactivate db
	else 拒絕
		man -> inform : 點擊叉叉\n 該通知不見
	end
	@enduml

活動群組討論及發表
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

活動群組討論

.. uml::
	
	@startuml
	actor User order 1
	participant ":活動群組討論頁面" as main order 2
	participant ":討論主題子頁面" as A order 3
	participant ":便利貼子頁面" as B order 4
	participant ":聊天窗子頁面" as C order 5
	participant ":發表便利貼子頁面" as D order 6
	database Data as db order 100

	activate db
	activate A
	activate B
	activate C
	activate D
	opt 聊天
		User -> C : 輸入聊天訊息
		User -> C : 點擊 送出\n click sendChatMessageBtn
		C -> db : sendChatMessage(Message, User.sName)
		C <- db : [if any update]\nretrive sLastestChatMessage
	else 發表便利貼
		User -> D : 撰寫便利貼內容
		User -> D : 點擊 送出\n click send3MMessageBtn
		D -> db : send3MMessage(Message, User,sName)
		D <- db : [if any update]\nretrive sLastest3MMessage
	else 編輯便利貼
		User -> B : 勾選便利貼
		User -> B : 點擊 新增至子標題按鈕\n click addToSubtitleBtn
		B -> User : 跳出選擇子標題的畫面\n whichSubtitleToAdd()
		User -> B : 選擇欲新增至的討論子標題
		B -> db : addToSubtitle(sContent)
		B <- db : [if any update]\nretrive update Page
	else 新增討論子主題
		User -> A : 點擊 新增按鈕\n click addNewSubtitleBtn
		A -> User : 跳出輸入子標題名稱畫面\n fillSubtitleName()
		User -> A : 輸入欲新增的子標題名稱
		A -> db : addNewSubtitle(sSubtitle)
		A <- db : [if any update]\nretrive update Page
	end
	@enduml

活動群組討論-聊天

.. uml::
	
	@startuml
	actor User order 1
	participant ":活動群組討論頁面" as main order 2
	'participant ":討論主題子頁面" as A order 3
	'participant ":便利貼子頁面" as B order 4
	participant ":聊天窗子頁面" as C order 5
	'participant ":發表便利貼子頁面" as D order 6
	database Data as db order 100

	activate db
	activate C
	title 聊天
		User -> C : 輸入聊天訊息
		User -> C : 點擊 送出\n click sendChatMessageBtn
		C -> db : sendChatMessage(Message, User.sName)
		C <- db : [if any update]\nretrive sLastestChatMessage
	
	@enduml

活動群組討論-便利貼

.. uml::
	
	@startuml
	actor User order 1
	participant ":活動群組討論頁面" as main order 2
	'participant ":討論主題子頁面" as A order 3
	participant ":便利貼子頁面" as B order 4
	'participant ":聊天窗子頁面" as C order 5
	participant ":發表便利貼子頁面" as D order 6
	database Data as db order 100

	activate db
	'activate A
	activate B
	'activate C
	activate D
	opt 發表便利貼
		User -> D : 撰寫便利貼內容
		User -> D : 點擊 送出\n click send3MMessageBtn
		D -> db : send3MMessage(Message, User,sName)
		D <- db : [if any update]\nretrive sLastest3MMessage
	else 編輯便利貼
		User -> B : 勾選便利貼
		User -> B : 點擊 新增至子標題按鈕\n click addToSubtitleBtn
		B -> User : 跳出選擇子標題的畫面\n whichSubtitleToAdd()
		User -> B : 選擇欲新增至的討論子標題
		B -> db : addToSubtitle(sContent)
		B <- db : [if any update]\nretrive update Page
	end
	@enduml

活動群組討論-討論子主題

.. uml::
	
	@startuml
	actor User order 1
	participant ":活動群組討論頁面" as main order 2
	participant ":討論主題子頁面" as A order 3
	'participant ":便利貼子頁面" as B order 4
	'participant ":聊天窗子頁面" as C order 5
	'participant ":發表便利貼子頁面" as D order 6
	database Data as db order 100

	activate db
	activate A
	'activate B
	'activate C
	'activate D

	title 新增討論子主題
		User -> A : 點擊 新增按鈕\n click addNewSubtitleBtn
		A -> User : 跳出輸入子標題名稱畫面\n fillSubtitleName()
		User -> A : 輸入欲新增的子標題名稱
		A -> db : addNewSubtitle(sSubtitle)
		A <- db : [if any update]\nretrive update Page
	
	@enduml	

活動群組發表

.. uml::
	
	@startuml
	actor User order 1
	participant ":活動群組討論頁面" as main order 2	
	participant ":群組成果頁面" as result order 3
	database Data as db order 100

	activate main
	User -> main : 點擊 發表成果\n click publishBtn
	main -> db : publish(sGroupTitle)
	activate db
	main -> result : <<create>>
	deactivate main
	activate result
	result -> db : retrive data
	result <-- db : render data
	deactivate db
	result -> User : direct to 成果頁面
	deactivate result
	@enduml

====================================================================
v0.3-20190922 add 履歷管理系統, 活動群組
====================================================================

--------------------------------------------------------------------
活動群組
--------------------------------------------------------------------

設立活動群組
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. uml::

	@startuml
	actor User order 1
	participant ":活動群組版塊" as page order 2
	participant ":活動群組資料填寫頁面" as info order 3
	participant ":活動群組討論頁面" as main order 4
	database Data as db order 100

	activate page
	User -> page : 點擊 設立活動群組
	page -> info : <<create>>
	deactivate page
	activate info
	info -> User : show page
	User -> info : 填寫資料
	User -> info : click OK
	info -> db : [if !checkExistSameName(group.title)]\n store()
	activate db
	db -> info : <<create 活動群組 successfully>>
	info -> main : <<create>> group.title = title
	deactivate info
	activate main
	main -> db : retrive data (group.title)
	db -> main : render data
	deactivate db
	main -> User : direct to 活動群組討論頁面
	@enduml

加入活動成員
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. uml::
	
	@startuml
	actor User order 1
	participant ":活動群組頁面" as page order 2
	participant ":搜尋使用者頁面" as se order 3
	database Data as db order 100

	activate page
	User -> page : click 加入活動成員
	page -> se : <<create>>
	activate se
	se -> User : show page
	opt 搜尋人名
		User -> se : 輸入人名
		se -> db : find Users
		activate db
		db --> se : User.Name
		deactivate db
		User -> se : 選擇要加入的人
		User -> se : 點擊 送出邀請 
	else 複製連結
		User -> se : 點擊 複製連結
	end

	@enduml

加入活動群組
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. uml::
	
	@startuml
	actor User order 1
	participant ":通知頁面" as inform order 2
	participant ":活動群組版塊" as page order 3
	database Data as db order 100

	alt 收到群組邀請
		User -> inform : 點擊接受
		activate inform
		inform -> db : 活動群組.member.add(User.name)
		inform --> User : show message \n已成功加入活動群組
		deactivate inform
	else 點擊群組邀請網址
		User -> inform : 點擊接受
		activate inform
		inform -> db : 活動群組.member.add(User.name)
		inform --> User : show message \n已成功加入活動群組
		deactivate inform
	else 自己申請加入

		User -> page : 搜尋欲加入的群組
		User -> page : [if 活動群組 is public]\n 點擊加入
		page -> User : <<申請已送出>>
		ref over page, db : 申請加入
		... awaiting ...
		page -> User : inform User 成功加入／被拒絕加入
	end
	@enduml

ref-申請加入

.. uml::

	@startuml
	actor 群組管理人員 as man order 1
	participant ":通知頁面" as inform order 2
	database Data as db order 100

	activate inform
	inform -> man : 通知要求加入活動群組
	alt 同意
		man -> inform : 點擊同意
		inform -> db : 活動群組.member.add(User.name)
		activate db
		db --> inform : 已成功增加成員
		deactivate db
	else 拒絕
		man -> inform : 點擊拒絕
		inform --> man : show 拒絕某人加入message
	end
	@enduml

活動群組討論及發表
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

活動群組討論

.. uml::
	
	@startuml
	actor User order 1
	participant ":活動群組討論頁面" as main order 2
	participant ":討論主題子頁面" as A order 3
	participant ":便利貼子頁面" as B order 4
	participant ":聊天窗子頁面" as C order 5
	participant ":發表便利貼子頁面" as D order 6
	database Data as db order 100

	activate db
	activate A
	activate B
	activate C
	activate D
	opt 聊天
		User -> C : 輸入聊天訊息
		User -> C : 點擊 送出
		C -> db : group.chat.add(message)
		C <- db : [if added some message or any update]\nretrive lastest message
	else 發表便利貼
		User -> D : 撰寫便利貼內容
		User -> D : 點擊 送出
		D -> db : group.threeM.add(message)
		D <- db : [if added some message or any update]\nretrive lastest message
	else 編輯便利貼
		User -> B : 勾選便利貼
		User -> B : 點擊 新增按鈕
		User -> B : 選擇欲新增至的討論子標題
		B -> db : group.subtitle.add(threeM)
		B <- db : [if added some message or any update]\nretrive lastest message
	else 新增討論子主題
		User -> A : 點擊 新增按鈕
		User -> A : 輸入欲新增的子標題名稱
		A -> db : group.addSubtitle(name)
		A <- db : [if added some message or any update]\nretrive lastest message
	end
	@enduml

活動群組發表

.. uml::
	
	@startuml
	actor User order 1
	participant ":活動群組討論頁面" as main order 2	
	participant ":群組成果頁面" as result order 3
	database Data as db order 100

	activate main
	User -> main : 點擊 發表成果
	main -> db : save()
	activate db
	main -> result : <<create>>
	deactivate main
	activate result
	result -> db : retrive data
	result <-- db : render data
	deactivate db
	result -> User : direct to 成果頁面
	deactivate result
	@enduml

--------------------------------------------------------------------
履歷管理系統
--------------------------------------------------------------------

審查特定履歷
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. uml::

	@startuml
	actor User order 1
	participant ":總履歷介面" as page order 2
	database Data as db order 100

	activate page
	User -> page : [if 審查狀態 == 未認證]\n勾選欲審查的項目
	User -> page : 點擊送出審查
	page -> db : 將項目送入待審查
	activate db
	db -> page : set 項目狀態 = 待審查
	deactivate db

	ref over db : 管理員審查
	... awaiting 管理員審查 ...

	alt 審查通過
		db -> db : set 項目狀態 = 已認證
		activate db
		db -> page : [if User refresh the page ]\n更新項目狀態
		deactivate db
	else 審查不通過
		db -> db : set 項目狀態 = 不通過\n N天內禁止送出審查
		activate db
		db -> page : [if User refresh the page ]\n更新項目狀態
		deactivate db
	end
		page -> User : 通知審查結果

	@enduml

reference-管理員審查

.. uml::

	@startuml
	actor Manager as m order 1
	participant ":審查履歷介面" as page order 2
	database Data as db order 100

	activate db
	db -> page : 更新審查要求通知
	activate page
	deactivate db
	m -> page : 點擊未審查項目\n order by 同一個人 or 同一個項目
	alt 予以通過項目
		m -> page : 勾選通過項目
		m -> page : 點擊通過按鈕
		page -> db : set 項目狀態 = 已認證
		activate db
		page -> page : refresh 頁面
		deactivate db
	else 不予以通過項目
		m -> page : 勾選不通過項目
		m -> page : 填寫不通過原因
		m -> page : 點擊不通過按鈕
		page -> db : set 項目狀態 = 不通過\n N天內禁止送出審查
		page -> page : refresh 頁面
		deactivate db
	end


	@enduml

設定履歷
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

履歷總管-新增（填寫）履歷

.. uml::
	
	@startuml
	actor User order 1
	participant ":履歷總管" as all order 2
	database Data as db order 100

	User -> all : 進入履歷總管
	activate all
	User -> all : 填寫履歷內容
	User -> all : click 變更(儲存)
	all -> db : update data
	activate db
	db -> all : update successfully message
	deactivate db
	all -> User : 顯示變更成功

	@enduml

新增履歷-以及設定隱私

.. uml::
	
	@startuml
	actor User order 1
	participant ":履歷總覽" as A order 2
	participant ":新增履歷頁面" as B order 3
	participant ":特定履歷頁面" as C order 4
	database Data as db order 100

	User -> A : 進入履歷總覽
	activate A
	opt 新增履歷
		User -> A :點擊 新增履歷
		A -> B : <<create>>
		activate B
		B -> User : show page
		User -> B : 填寫履歷名稱
		User -> B :click 儲存
		B -> db : create 履歷表
		deactivate B
		activate db
	else 選擇欲設定的履歷
		User -> A :選擇欲設定的履歷
	end
	deactivate db
	C -> User : direct to 特定履歷表頁面
	activate C
	User -> C : 設定各項隱私 顯示／隱藏
	User -> C : click 儲存
	C -> db : update User.履歷

	@enduml



====================================================================
v0.1-20190912
====================================================================

1. :ref:`Seq_title1`
	#. :ref:`Seq_graph1_1`
	#. :ref:`Seq_graph1_2`
	#. :ref:`Seq_graph1_3`
#. :ref:`Seq_title2`

#. :ref:`Seq_title3`

#. :ref:`Seq_title4`
	#. :ref:`Seq_graph4_1`
#. :ref:`Seq_title5`

.. _Seq_title1:

--------------------------------------------------------------------
文章管理系統
--------------------------------------------------------------------

.. _Seq_graph1_1:

發表文章
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. uml::

	@startuml
	actor User order 1
	database Database as Data order 100
	participant ":發表文章按鈕" as btn order 2
	participant ":文章編輯頁面" as page order 3
	participant ":發文規則按鈕" order 4
	participant ":預覽文章按鈕" order 6
	
	participant ":儲存按鈕" order 90
	

	== Initialization ==
	User -> btn : click
	activate btn
	create page
	btn ->  page : createPage()
	activate page
	deactivate btn
	page -> Data : retriveEditngArtical(userAccount)
	activate Data
	return userAccount.editingArtical
	deactivate Data
	User <-- page : direct to artical editing page
	User -> page : 編輯內文

	== Editing Artical ==

	group 設定一般文章設定
	opt 查看發文規則
		User -> ":發文規則按鈕" : click
		activate ":發文規則按鈕" 
		create ":發文規則頁面" order 5
		":發文規則按鈕" -> ":發文規則頁面" : createPage()
		deactivate ":發文規則按鈕"
		activate ":發文規則頁面"
		":發文規則頁面" -> Data : retriveRule(板塊名稱)
		activate Data
		":發文規則頁面" <-- Data : 	板塊名稱.發文規則
		deactivate Data
		User <- ":發文規則頁面" : Rule page
		User -> ":發文規則頁面" : close()
		destroy ":發文規則頁面"
	else 預覽文章
		User -> ":預覽文章按鈕" : click
		activate ":預覽文章按鈕"
		create ":預覽文章頁面" order 7
		par
			":預覽文章按鈕" -> ":預覽文章頁面" : createPage()
			activate ":預覽文章頁面"
			":預覽文章按鈕" -> Data : save()
		end
		deactivate ":預覽文章按鈕"	
		":預覽文章頁面" -> Data : retriveEditngArtical(userAccount)
		activate Data
		return userAccount.editingArtical
		deactivate Data
		User <-- ":預覽文章頁面" : show 預覽文章頁面
		User -> ":預覽文章頁面" : close()
		destroy ":預覽文章頁面"
	end
	== Saving/Publishing Artical ==

	opt 儲存文章
	User -> ":儲存按鈕" : click
	activate ":儲存按鈕"
	":儲存按鈕" -> Data : save()
	note right
		saving all changes including:
		內文、板塊、標題、身份、閱讀權限
	end note
	activate Data
	return <<Response>>
	deactivate Data
	User <-- ":儲存按鈕" : showSaveSuccessfulMessage()
	deactivate ":儲存按鈕"
	end

	'create ":下一步按鈕" as next order 20
	'User -> next : click
	'activate next
	'next -> page : isAllColumnFilled()
	'next <-- page : <<Response>>

	User -> page : click Next button
	page -> page : isAllColumnFilled()
	activate page


	create ":編輯話題頁面" as tag order 21
	page -> tag : [if True] \ncreatePage()
	deactivate page
	activate tag
	User <-- tag : show 編輯話題 page
	'deactivate next
	User -> tag : 選擇或輸入話題標籤並按發布	
	tag -> Data : save()
	activate Data
	tag <-- Data : <<Response>>
	create ":文章頁面" as artical order 30
	tag -> artical : [if saved successfully]\ncreate()
	activate artical
	deactivate tag
	deactivate page
	artical -> Data : retriveArtical(artical's serial number)
	artical <-- Data : artical.serial_number
	deactivate Data
	User <-- artical : direct to artical page

	@enduml

發表文章-1

.. uml::

	@startuml
	actor User order 1
	database Database as Data order 100
	participant ":發表文章按鈕" as btn order 2
	participant ":文章編輯頁面" as page order 3

	title 發表文章-1
	== Initialization ==
	User -> btn : click
	activate btn
	create page
	btn ->  page : createPage()
	activate page
	deactivate btn
	page -> Data : retriveEditngArtical(userAccount)
	activate Data
	return userAccount.editingArtical
	deactivate Data
	User <-- page : direct to artical editing page
	User -> page : 編輯內文
	@enduml

發表文章-2

.. uml::

	@startuml
	actor User order 1
	database Database as Data order 100
	'participant ":發表文章按鈕" as btn order 2
	participant ":文章編輯頁面" as page order 3
	participant ":發文規則按鈕" order 4
	participant ":預覽文章按鈕" order 6
	
	participant ":儲存按鈕" order 90

	title 編輯時
	== Editing Artical ==
	activate page
	group 設定一般文章設定
	opt 查看發文規則
		User -> ":發文規則按鈕" : click
		activate ":發文規則按鈕" 
		create ":發文規則頁面" order 5
		":發文規則按鈕" -> ":發文規則頁面" : createPage()
		deactivate ":發文規則按鈕"
		activate ":發文規則頁面"
		":發文規則頁面" -> Data : retriveRule(板塊名稱)
		activate Data
		":發文規則頁面" <-- Data : 	板塊名稱.發文規則
		deactivate Data
		User <-- ":發文規則頁面" : Rule page
		User -> ":發文規則頁面" : close()
		destroy ":發文規則頁面"
	else 預覽文章
		User -> ":預覽文章按鈕" : click
		activate ":預覽文章按鈕"
		create ":預覽文章頁面" order 7
		par
			":預覽文章按鈕" -> ":預覽文章頁面" : createPage()
			activate ":預覽文章頁面"
			":預覽文章按鈕" -> Data : save()
		end
		deactivate ":預覽文章按鈕"	
		":預覽文章頁面" -> Data : retriveEditngArtical(userAccount)
		activate Data
		return userAccount.editingArtical
		deactivate Data
		User <-- ":預覽文章頁面" : show 預覽文章頁面
		User -> ":預覽文章頁面" : close()
		destroy ":預覽文章頁面"
	end
	@enduml

發表文章-3

.. uml::

	@startuml
	actor User order 1
	database Database as Data order 100
	'participant ":發表文章按鈕" as btn order 2
	participant ":文章編輯頁面" as page order 3
	'participant ":發文規則按鈕" order 4
	'participant ":預覽文章按鈕" order 6
	
	participant ":儲存按鈕" order 90

	title 儲存及發表
	== Saving/Publishing Artical ==
	activate page
	opt 儲存文章
	User -> ":儲存按鈕" : click
	activate ":儲存按鈕"
	":儲存按鈕" -> Data : save()
	note right
		saving all changes including:
		內文、板塊、標題、身份、閱讀權限
	end note
	activate Data
	return <<Response>>
	deactivate Data
	User <-- ":儲存按鈕" : showSaveSuccessfulMessage()
	deactivate ":儲存按鈕"
	end

	'create ":下一步按鈕" as next order 20
	'User -> next : click
	'activate next
	'next -> page : isAllColumnFilled()
	'next <-- page : <<Response>>

	User -> page : click Next button
	page -> page : isAllColumnFilled()
	activate page


	create ":編輯話題頁面" as tag order 21
	page -> tag : [if True] \ncreatePage()
	deactivate page
	activate tag
	User <-- tag : show 編輯話題 page
	'deactivate next
	User -> tag : 選擇或輸入話題標籤並按發布	
	tag -> Data : save()
	activate Data
	tag <-- Data : <<Response>>
	create ":文章頁面" as artical order 30
	tag -> artical : [if saved successfully]\ncreate()
	activate artical
	deactivate tag
	deactivate page
	artical -> Data : retriveArtical(artical's serial number)
	artical <-- Data : artical.serial_number
	deactivate Data
	User <-- artical : direct to artical page

	@enduml

.. _Seq_graph1_2:

瀏覽文章
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

瀏覽文章-1

.. uml::
	
	@startuml
	actor User order 1
	participant ":瀏覽文章頁面" as page order 2
	database Data as db order 100

	User -> page : click
	activate page
	page -> db : retriveArtical(artical.serial_number)
	activate db
	page <-- db : artical.content
	deactivate db
	User <- page : show page

	@enduml

瀏覽文章-於文章留言

.. uml::
	
	@startuml
	actor User order 1
	participant ":瀏覽文章頁面" as page order 2
	database Data as db order 100

	activate page

	User -> page : 撰寫留言內容
	User -> page : click 送出留言
	page -> db : send comment
	activate db
	page <-- db : updatePage()
	deactivate db
	User <- page : show comment successfully sent message

	@enduml

瀏覽文章-於文章按讚

.. uml::
	
	@startuml
	actor User order 1
	participant ":瀏覽文章頁面" as page order 2
	database Data as db order 100

	activate page

	User -> page : click 讚
	page -> db : [if 該用戶尚未對此文章按讚過] \nartical.serial_number.like += User
	activate db
	db -> db : artical.serial_number.countLike()
	activate db
	page <-- db : update()
	deactivate db
	User <- page : update like number

	@enduml

瀏覽文章-收藏文章

.. uml::
	
	@startuml
	actor User order 1
	participant ":瀏覽文章頁面" as page order 2
	database Data as db order 100

	activate page

	User -> page : click 收藏
	page -> db : User.收藏(artical.serial_number)
	activate db
	db -> db : artical.serial_number.收藏 += User
	activate db
	page <-- db : artical.serial_number.count收藏數()
	deactivate db
	User <- page : update 收藏數
	User <- page : show 收藏成功

	@enduml

瀏覽文章-檢舉文章

.. uml::
	
	@startuml
	actor User order 1
	participant ":瀏覽文章頁面" as page order 2
	database Data as db order 100

	activate page

	User -> page : click 檢舉
	create ":檢舉畫面" order 3
	page -> ":檢舉畫面"
	User <-- ":檢舉畫面" : show 檢舉畫面
	User -> ":檢舉畫面" : 選擇並填寫原因
	User -> ":檢舉畫面" : 送出檢舉
	":檢舉畫面" -> db : artical.serial_number.檢舉 新增檢舉
	page <- db : show 檢舉成功送出
	User <- page : show 檢舉成功送出
	@enduml

.. _Seq_graph1_3:

審查檢舉文章
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

審查檢舉文章

.. uml::
	
	@startuml
	actor Manager order 1
	database Data as db order 100
	participant ":檢舉審查頁面"  as page order 2
	actor 被檢舉人 as qq order 101
	actor 檢舉人 as teller order 102

	activate db
	db -> page : [if received 檢舉]\n 顯示有新的檢舉
	activate page
	deactivate db
	Manager <- page : [if 顯示有新的檢舉]\n inform Manager
	Manager -> page : 查看待審查的檢舉
	alt 檢舉屬實
		Manager -> page : click 檢舉屬實
		create ":懲戒及填寫原因頁面" as why order 4
		page -> why : <<create>>
		activate why
		Manager <- why : show page
		Manager -> why : 填寫原因、選擇處分
		Manager -> why : click 送出
		why -> db : set 檢舉結果
		deactivate why
		activate db

	else 檢舉駁回
		Manager -> page : click 檢舉駁回
		create ":填寫檢舉駁回原因頁面" as what order 5
		page -> what : <<create>>
		activate what
		Manager <- what : show page
		Manager -> what : 填寫原因
		Manager -> what : click 送出
		what -> db : set 檢舉結果
		deactivate what
		
	end
	db -> db : set 檢舉.status = 已審查
	activate db
	page <- db : updatePage()
	deactivate db

	db -> qq : 通知並處分
	db -> teller : 通知檢舉結果

	@enduml

審查檢舉文章-被檢舉人提交複查

.. uml::
	
	@startuml
	actor 被檢舉人 as qq order 1
	actor Manager order 3
	participant ":檢舉通知頁面"  as qqpage order 2
	participant ":檢舉審查頁面"  as Manpage order 4
	database Data as db order 100

	activate qqpage
	qqpage -> qq : 通知檢舉結果
	qq -> qqpage : click 提交複查
	qq -> qqpage : 填寫複查原因
	qq -> qqpage : 送出
	qqpage -> db : 新增檢舉複查
	activate db
	db -> Manpage : 更新複查案
	deactivate db
	activate Manpage
	Manpage -> Manager : 通知複查
	deactivate Manpage
	alt 變更處分
		Manager -> Manpage : 變更處分
		activate Manpage
	else 維持處分
		Manager -> Manpage : 維持處分
	end
	Manpage -> db : update 處分
	activate db
	db -> qq : inform 複查結果
	deactivate db


	@enduml

.. _Seq_title2:

--------------------------------------------------------------------
履歷管理系統
--------------------------------------------------------------------

.. _Seq_title3:

--------------------------------------------------------------------
好友訊息系統
--------------------------------------------------------------------

.. _Seq_title4:

--------------------------------------------------------------------
權限管理系統
--------------------------------------------------------------------

.. _Seq_graph4_1:

使用者身份異動
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

使用者新增身份

.. uml::
	
	@startuml
	actor User order 1
	participant ":履歷管理頁面" as page order 2
	database Data as db order 100

	activate page
	User -> page : 點選新增身份按鈕
	create ":新增身份畫面" as id order 3
	page -> id : << create >>
	activate id
	User <-- id : show 新增身份畫面
	User -> id : 選擇欲新增的身份
	note right
		7+1種身份
	end note
	opt 選擇身份年限
		User -> id : 選擇身份年限
	end
	User -> id : 上傳身份佐證
	id -> db : upload files
	User -> id : 點擊送出審查
	deactivate id

	ref over id, db : 管理員審查身份
	...  awaiting 管理員審查 ...

	alt 審查通過
		page <- db : updateStatus()
		User <- page : 顯示該身份通過審核
	else 審查不通過
		page <- db : updateStatus()
		User <- page : 顯示該身份不通過審核
		opt 查看不通過原因
			User -> page : 點擊不通過原因
			return : 不通過原因
		end
	else 審查中
		page <- db : [while user update page]\nupdateStatus()
		User <- page : 顯示該身份審核中
		opt 重送審核
			User -> page : 點擊重送審核
			ref over page, db : 管理員審查身份
			note right
				24小時內只能重新送出乙次
			end note
		end
	end

	@enduml

.. uml::
	
	@startuml
	title ref 管理員審查身份
	actor Manager order 1
	participant ":履歷審查頁面" as page order 2
	database Data as db order 100

	activate page
	page <- db : [while 有新的送審要求]\n新的審查資料
	Manager -> page : 查閱待審資料
	alt 審查屬實
		Manager -> page : 點擊通過審查
	else 審查有疑慮（不通過）
		Manager -> page : 填寫不通過原因
		Manager -> page : [if 不通過原因 is filled]\n點擊不通過審查
	end
	page -> db : updateStatus()

	@enduml

.. _Seq_title5:

--------------------------------------------------------------------
寶物管理系統
--------------------------------------------------------------------