.. note::
	- Start: 20171209
	- System Environment:
		- Python v3.6.3
		- plantuml.jar v1.2018.10
		- graphviz v0.9 (v2.38)
		
####################################################################
Class Diagram
####################################################################


********************************************************************
Change Log
********************************************************************
A. v0.1 20190927 drafted by LKY
#. v0.2 20190928 drafted by LKY
#. v0.3 20190930 revise 活動、履歷, add 文章、身份

********************************************************************
Diagrams
********************************************************************

====================================================================
v0.3 20190930 revise 活動、履歷, add 文章、身份
====================================================================

--------------------------------------------------------------------
文章
--------------------------------------------------------------------


.. uml::
	
	@startuml
	skinparam classAttributeIconSize 0
	class User{
		-sName : String
		-sIdentification_身份 : String
		-sAccount : String
		-sPassword : String
		-sMyKeepsArtical : String list
		-sMyPublishedArtical : String list
		-sMyComment : String list
		void setAccount()
		void forgetAccount()

	}
	class ArticleEdit{
		-sContent : String
		-iSerialNumberOfArticle : integer
		articleRuleBtn : button
		sArticleRule : String
		void createArticleRulePage(sBoardName)
		void getArticleRule(sBoardName)
		previewBtn : button
		void createPreviewPage()
		void saveArticle(iSerialNumberOfArticle)
		saveArticleBtn : button
		void saveArticle(iSerialNumberOfArticle, sContent)
		nextBtn : button
		boolean isAllColumnFilled()
		void createArticleTopicsPageBtn()
	}
	class ArticlePreview extends Article{
		-sContent : String
	}
	class ArticleTopics extends ArticlePreview{
		void setTopics()
		publishBtn : button
		void publish(iSerialNumberOfArticle)
	}
	class Article{
		'-iStatus : integer
		-iSerialNumberOfArticle : integer, primary key
		-sTitle : String
		-sContent : String
		-iNumberOfLikes : integer
		-sWhoLikes : String list	
		-s所在板塊 : String
		-sUserInfo : String
		-sContentOfComment : String
		-sTopics :String
		commentBtn : button
		likeBtn : button
		-iLikeStatus : integer
		collectBtn : button
		-sWhoCollect : String list
		void setWhoCollect(User.sAccount)
		-iCollectStatus : integer
		reportBtn : button
		void createReportPage(iSerialNumberOfArticle, User.sAccount)
		void getLikeStatus(User.sAccount)
		void getCollectStatus(User.sAccount)
		void sendComment(sContentOfComment)
		void setWhoLikes(User.sName)
		void setNumberOfLikes()
		void set所在板塊()
		void getUserInfo()
		#sReportRecord : list

	}
	class CommentOfArticle{
		-iNumber : integer
		-sContentOfComment : String
		void setComment(User.sName, sContentOfComment, date)
	}
	class Report{
		-iReportNumber : integer
		-iReportStatus : integer
		-iClassification : integer
		-sResaons : String
		-sWho : String
		sendReportBtn : button
		setReportRecord(iSerialNumberOfArticle, iClassification)
		reportAcceptBtn : button
		-sCheckResult : String
		void setCheckResult(sCheckResult)
		void createPunishPage()
		reportRejectBtn : button
		void reportReject()
		void setReportStatus()
		void inform(User.sAccount of 檢舉人, User.sAccount of 被檢舉人)
	}
	class Punish extends Report{
		sPunishment : list
		punishBtn : button
		punish()
	}
	class Inform{
		void inform(User.sAccount)
		reportObjectBtn : button
		void reportObject(sReportObjection)
		-sReportObjection : String
	}

	class Board{
		sBoardName : String
		editArticleBtn : button
		void createEditArticlePage(sBoardName)
		void getArtical(Artical.s所在板塊)
		void setTopArtical(Artical.sTitle)
	}
	CommentOfArticle "0..*" --* "1" Article
	Board "1" o-- "0..*" Article
	Article --> Report : user >
	ArticleEdit --> Article : create >
	User "1" o-- "0..*" Article
	Report --> Inform : use >

	@enduml

--------------------------------------------------------------------
履歷管理
--------------------------------------------------------------------

.. uml::
	
	@startuml
	skinparam classAttributeIconSize 0
	class User{
		-sName : String
		-sIdentification_身份 : String
		-sAccount : String
		-sPassword : String
		-sMyKeepsArtical : String list
		-sMyPublishedArtical : String list
		-sMyComment : String list
		void setAccount()
		void forgetAccount()

	}

	class UserInfo{
		-sNickname_暱稱 : String
		-iArticalNumber_文章數 : integer
		-iScore_積分 : integer
		-iReadingScore_閱讀權限 : integer
		-sRegisterDate_註冊日期 : Date
		-iExistDays_註冊天數 : integer
		-iMissingDays_未上線天數 : integer
		-sShortIntroduction_短自介 : String
	}

	class TotalCV{
		-sName : String
		-sStudentID : String
		-sNickName : String
		-sSex : String
		-sIdentification_身份 : String
		-sPhoneNumber1 : String
		-sPhoneNumber2 : String
		-sPhoneNumber3 : String
		-sAddressHome : String
		-sAddressCompany : String
		-sEmail : String
		-bolbAttachment : BLOB
		saveCVBtn : button
		void getSpecificCV(sIdentification_身份)
		String saveCV()
		newCVBtn : button
		void newCV()
		newIdentificationBtn
		void newIdentification(User.sAccount)
	}
	class Identification extends TotalCV{
		blobProof : BLOB
		void uploadFile()
		checkBtn : button
		void check()
		-iStatus : integer
		-sCheckResult
		String showCheckResult()
		void getStatus()
		void resendRequest()
		void setCheckResult()
		void setStatus()
	}


	class SpecificCV extends TotalCV{
		-sTitle : String
		-sDescription : String
		-iSerialNumber : integer
		-blobProofAttachment : BLOB
		-iStatus : Integer
		submitBtn : button
		String submit(iSerialNumber)
		+ void getiStatus(iSerialNumber)
		void informUserResult(iSerialNumber, iStatus)
	}

	class createMyCV{
		sCVName : String
		void saveAndCreateCV(sCVName)
	}

	class MyCV extends TotalCV{
		sCVName : String
		void saveMyCV()
		-bShowStudentID : boolean
		-bShowNickName : boolean
		-bShowSex : boolean
		-bShowIdentification : boolean
		-bShowPhoneNumber1 : boolean
		-bShowPhoneNumber2 : boolean
		-bShowPhoneNumber3 : boolean
		-bShowAddressHome : boolean
		-bShowAddressCompany : boolean
		-bShowEmail : boolean

	}

	class ExamineCV{
		updateExamineeCVBtn : button
		void updateExamineeCV()
		passCVBtn : button
		void passCV(SpeicalCV.iSerialNumber)
		noPassCVBtn : button
		void noPassCV(resaon, SpeicalCV.iSerialNumber)

	}

	User "1" *- "1" UserInfo
	User "1" *- "1" TotalCV
	'TotalCV "1" *-- "0..*" SpecificCV
	User "1" *-- "1..*" MyCV
	createMyCV --> MyCV : create >
	SpecificCV --> ExamineCV : user >
	@enduml

--------------------------------------------------------------------
活動群組
--------------------------------------------------------------------

.. uml::
	
	@startuml
	skinparam classAttributeIconSize 0
	class GroupBoard extends Board{
		createGroupBtn: button
		void createGroupInfo(User.sName)
	}

	class Group{
		String addMember(User.sName)
		sGroupTitle : String
		sGroupMember : list
		iStatus : integer
	}

	class CreateGroupInfoPage extends Group{
		OKBtn : button
		boolean bCheckExistSameName(sGroupTitle)
		String store({sGroupTitle, User.sName, })
		String showSameTitleMessage()
		String showCreateSucessfully()
		void createPage(sGroupTitle)
	}
	class GroupPage extends Group{
		dict getGroupInfo(sGroupTitle)
		void redirect(sGroupTitle)
		addMemberBtn : button
		void createSearchPage()
		JoinBtn : button
		String JoinGroup(User.sName)
		String informGroupInvite(Group.sGroupTitle, User.sName)
		publishBtn : button
		void publish(sGroupTitle)

	}
	class GroupSearchPage extends Group{
		list findUser(User.sName)
		invite : button
		String invite(User.sName)
		createURLbtn : button
		String createURL(sGroupTitle)
	}

	class GroupChat{
		void sendChatMessage(Message)
		sLastestChatMessage : string
	}

	class Group3M{
		sContent : String
		send3MMessageBtn : button
		sendChatMessage(Message, User.sName)
		sLastest3MMessage : String
		moveToSubtitleBtn : button
		somePage whichSubtitleToAdd()
		void addToSubtitle(sContent)
	}
	class GroupSubtitle{
		sSubtitle : String
		addNewSubtitleBtn : button
		void fillSubtitleName()
	}

	
	GroupBoard "1" o-- "0 .. *" Group
	GroupPage --> UserInform : use >
	User "1" --> "0 .. *" Group 
	Group "1" --> "1 .. *"User 
	GroupPage "1" *-- "1" GroupChat
	GroupPage "1" *-- "1" Group3M
	GroupPage "1" *-- "1" GroupSubtitle

	@enduml

====================================================================
v0.2 20190928 drafted by LKY
====================================================================

.. note::
	
	#. done 活動群組
	#. 履歷not yet

履歷管理
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. uml::
	
	@startuml
	skinparam classAttributeIconSize 0
	class User{
		-sName : String
		-sIdentification_身份 : String
		-sAccount : String
		-sPassword : String
		-sMyKeepsArtical : String list
		-sMyPublishedArtical : String list
		-sMyComment : String list

		void setAccount()
		void forgetAccount()

	}

	class UserInfo{
		-sNickname_暱稱 : String
		-iArticalNumber_文章數 : integer
		-iScore_積分 : integer
		-iReadingScore_閱讀權限 : integer
		-sRegisterDate_註冊日期 : Date
		-iExistDays_註冊天數 : integer
		-iMissingDays_未上線天數 : integer
		-sShortIntroduction_短自介 : String
	}

	class TotalCV{
		-sName : String
		-sStudentID : String
		-sNickName : String
		-sSex : String
		-sIdentification_身份 : String

		-sPhoneNumber1 : String
		-sPhoneNumber2 : String
		-sPhoneNumber3 : String
		-sAddressHome : String
		-sAddressCompany : String
		-sEmail : String

		-bolbAttachment : BLOB

		void getSpecificCV(sIdentification_身份)
	}

	class SpecificCV{
		-sDescription : String
		-blobProofAttachment : BLOB
	}

	class MyCV extends TotalCV{
		-bShowStudentID : boolean
		-bShowNickName : boolean
		-bShowSex : boolean
		-bShowIdentification : boolean

		-bShowPhoneNumber1 : boolean
		-bShowPhoneNumber2 : boolean
		-bShowPhoneNumber3 : boolean
		-bShowAddressHome : boolean
		-bShowAddressCompany : boolean
		-bShowEmail : boolean

	}

	User "1" *- "1" UserInfo
	User "1" *- "1" TotalCV
	TotalCV "1" *-- "0..*" SpecificCV
	User "1" *-- "1..*" MyCV
	@enduml

活動群組
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. uml::
	
	@startuml
	skinparam classAttributeIconSize 0
	class GroupBoard extends Board{
		createGroupBtn: button
		void createGroupInfo(User.sName)

	}

	class Group{
		String addMember(User.sName)
		sGroupTitle : String
		sGroupMember : list
		iStatus : integer

	}

	class CreateGroupInfoPage extends Group{
		
		OKBtn : button
		boolean bCheckExistSameName(sGroupTitle)
		String store({sGroupTitle, User.sName, })
		String showSameTitleMessage()
		String showCreateSucessfully()
		void createPage(sGroupTitle)
	}
	class GroupPage extends Group{
		dict getGroupInfo(sGroupTitle)
		void redirect(sGroupTitle)
		addMemberBtn : button
		void createSearchPage()
		JoinBtn : button
		String JoinGroup(User.sName)
		String informGroupInvite(Group.sGroupTitle, User.sName)
		publishBtn : button
		void publish(sGroupTitle)

	}
	class GroupSearchPage extends Group{
		list findUser(User.sName)
		invite : button
		String invite(User.sName)
		createURLbtn : button
		String createURL(sGroupTitle)
	}

	class GroupChat extends GroupPage{
		void sendChatMessage(Message)
		sLastestChatMessage : string
	}

	class Group3M extends GroupPage{
		sContent : String
		send3MMessageBtn : button
		sendChatMessage(Message, User.sName)
		sLastest3MMessage : String

		moveToSubtitleBtn : button
		somePage whichSubtitleToAdd()
		void addToSubtitle(sContent)
	}
	class GroupSubtitle extends GroupPage{
		sSubtitle : String
		addNewSubtitleBtn : button
		void fillSubtitleName()


	}

	
	GroupBoard "1" o-- "0 .. *" Group
	GroupPage --> UserInform : use >
	User "1" --> "0 .. *" Group 
	Group "1" --> "1 .. *"User 


	@enduml

總圖
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. uml::

	@startuml
	skinparam classAttributeIconSize 0
	class User{
		-sName : String
		-sIdentification_身份 : String
		-sAccount : String
		-sPassword : String
		-sMyKeepsArtical : String list
		-sMyPublishedArtical : String list
		-sMyComment : String list

		void setAccount()
		void forgetAccount()

	}

	class UserInfo{
		-sNickname_暱稱 : String
		-iArticalNumber_文章數 : integer
		-iScore_積分 : integer
		-iReadingScore_閱讀權限 : integer
		-sRegisterDate_註冊日期 : Date
		-iExistDays_註冊天數 : integer
		-iMissingDays_未上線天數 : integer
		-sShortIntroduction_短自介 : String
	}

	class TotalCV{
		-sName : String
		-sStudentID : String
		-sNickName : String
		-sSex : String
		-sIdentification_身份 : String

		-sPhoneNumber1 : String
		-sPhoneNumber2 : String
		-sPhoneNumber3 : String
		-sAddressHome : String
		-sAddressCompany : String
		-sEmail : String

		-bolbAttachment : BLOB

		void getSpecificCV(sIdentification_身份)
	}

	class SpecificCV{
		-sDescription : String
		-blobProofAttachment : BLOB
	}

	class MyCV extends TotalCV{
		-bShowStudentID : boolean
		-bShowNickName : boolean
		-bShowSex : boolean
		-bShowIdentification : boolean

		-bShowPhoneNumber1 : boolean
		-bShowPhoneNumber2 : boolean
		-bShowPhoneNumber3 : boolean
		-bShowAddressHome : boolean
		-bShowAddressCompany : boolean
		-bShowEmail : boolean

	}

	class Article{
		-sTitle : String
		-sContent : String
		-iNumberOfLikes : integer
		-sWhoLikes : String list
		-iNumberOfKeeps : integer
		-s所在板塊 : String
		-sUserInfo : String
		-sComment : String
		-sTags :String


		void setWhoLikes(User.sName)
		void setNumberOfLikes()
		void setNumberOfKeeps()
		void set所在板塊()
		void getUserInfo()
		void setComment(User.sName, comment, date)
		void report()
		void setTag()

	}
	class report{
		-iClassification : integer
		-sResaons : String
		-sWho : String
	}

	class Board{
		void getArtical(Artical.s所在板塊)
		void setTopArtical(Artical.sTitle)
	}

	class GroupBoard extends Board{
		createGroupBtn: button

		void createGroupInfo(User.sName)

	}

	class Group{
		String addMember(User.sName)
		sGroupTitle : String
		sGroupMember : list
		iStatus : integer

	}

	class CreateGroupInfoPage extends Group{
		
		OKBtn : button

		boolean bCheckExistSameName(sGroupTitle)
		String store({sGroupTitle, User.sName, })
		String showSameTitleMessage()
		String showCreateSucessfully()
		void createPage(sGroupTitle)
	}
	class GroupPage extends Group{
		dict getGroupInfo(sGroupTitle)
		void redirect(sGroupTitle)

		addMemberBtn : button
		void createSearchPage()

		JoinBtn : button
		String JoinGroup(User.sName)
		String informGroupInvite(Group.sGroupTitle, User.sName)


	}
	class GroupSearchPage extends Group{
		list findUser(User.sName)
		invite : button
		String invite(User.sName)
		createURLbtn : button
		String createURL(sGroupTitle)
	}

	class UserInform{
		acceptBtn : button
		String inform(sMessage, User.sName)

	}

	class GroupChat extends GroupPage{
		void sendChatMessage(Message)
		sLastestChatMessage : string
	}

	class Group3M extends GroupPage{
		sContent : String
		send3MMessageBtn : button
		sendChatMessage(Message, User.sName)
		sLastest3MMessage : String

		moveToSubtitleBtn : button
		somePage whichSubtitleToAdd()
		void addToSubtitle(sContent)
	}
	class GroupSubtitle extends GroupPage{
		sSubtitle : String
		addNewSubtitleBtn : button
		void fillSubtitleName()


	}

	User "1" *- "1" UserInfo
	User "1" *- "1" TotalCV
	TotalCV "1" *-- "0..*" SpecificCV
	User "1" *-- "1..*" MyCV
	Article -> report : use >
	User "1" o-- "0..*" Article
	Board "1" o-- "1" Article
	UserInform --* User
	GroupBoard "1" o-- "0 ..*" Group

	@enduml

====================================================================
v0.1 20190927 drafted by LKY
====================================================================

.. uml::

	@startuml
	'skinparam classAttributeIconSize 0
	class User{
		-sName : String
		-sIdentification_身份 : String
		-sAccount : String
		-sPassword : String
		-sMyKeepsArtical : String list
		-sMyPublishedArtical : String list
		-sMyComment : String list

		void setAccount()
		void forgetAccount()

	}

	class UserInfo{
		-sNickname_暱稱 : String
		-iArticalNumber_文章數 : integer
		-iScore_積分 : integer
		-iReadingScore_閱讀權限 : integer
		-sRegisterDate_註冊日期 : Date
		-iExistDays_註冊天數 : integer
		-iMissingDays_未上線天數 : integer
		-sShortIntroduction_短自介 : String
	}

	class TotoalCV{
		-sName : String
		-sStudentID : String
		-sNickName : String
		-sSex : String
		-sIdentification_身份 : String

		-sPhoneNumber1 : String
		-sPhoneNumber2 : String
		-sPhoneNumber3 : String
		-sAddressHome : String
		-sAddressCompany : String
		-sEmail : String

		-bolbAttachment : BLOB

		void getSpecificCV(sIdentification_身份)
	}

	class SpecificCV{
		-sDescription : String
		-blobProofAttachment : BLOB
	}

	class MyCV extends TotoalCV{
		-bShowStudentID : boolean
		-bShowNickName : boolean
		-bShowSex : boolean
		-bShowIdentification : boolean

		-bShowPhoneNumber1 : boolean
		-bShowPhoneNumber2 : boolean
		-bShowPhoneNumber3 : boolean
		-bShowAddressHome : boolean
		-bShowAddressCompany : boolean
		-bShowEmail : boolean

	}

	class Artical{
		-sTitle : String
		-sContent : String
		-iNumberOfLikes : integer
		-sWhoLikes : String list
		-iNumberOfKeeps : integer
		-s所在板塊 : String
		-sUserInfo : String
		-sComment : String
		-sTags :String


		void setWhoLikes(User.sName)
		void setNumberOfLikes()
		void setNumberOfKeeps()
		void set所在板塊()
		void getUserInfo()
		void setComment(User.sName, comment, date)
		void report()
		void setTag()

	}
	class report{
		-iClassification : integer
		-sResaons : String
		-sWho : String
	}

	class 版{
		void getArtical(Artical.s所在板塊)
		void setTopArtical(Artical.sTitle)
	}

	User "1" *- "1" UserInfo
	User "1" *- "1" TotoalCV
	TotoalCV "1" *-- "0..*" SpecificCV
	User "1" *-- "1..*" MyCV
	Artical -> report : use >
	User "1" o-- "0..*" Artical
	版 "1" o-- "1" Artical
	@enduml
	
	