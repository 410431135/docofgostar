.. note::
	- Start: 20171209
	- System Environment:
		- Python v3.6.3
		- plantuml.jar v1.2018.10
		- graphviz v0.9 (v2.38)
		
####################################################################
Entity Relationship Diagram
####################################################################


********************************************************************
Change Log
********************************************************************
A. v0.1 20191013 drafted by LKY
#. v0.2 20191016 change article topic
#. v0.3 20191018 add many to many between user and activity group
#. v0.4 20191020 deal with article collect and likes

********************************************************************
Diagrams
********************************************************************

====================================================================
v0.4 20191020 deal with article collect and likes
====================================================================

.. uml::

	@startuml
	hide circle
	skinparam linetype ortho

	'change accuont from string to integer and make it primary key
	'change identification to number for which represent a id by 1 and 0 if not possess one.
	entity "User" as user {
		*sName : text
		*iIdentification : 8-digits number, default 00000000
		*iAccount : number <<PK>>
		*sPassword : String
	}
	'bring out attachment to new class
	entity "TotalCV" as totalcv {
		*TotalCV_ID : integer <<PK>>
		*iUserAccount : integer, <<FK>>
		*sName : text
		iStudentID : number
		sNickname : text
		iSex : integer
		sIdentification : String
		sPhoneNumber1 : String
		sPhoneNumber2 : String
		sPhoneNumber3 : String
		sAddressHome : text
		sAddressCompany : text
		sEmail : text
	}
	entity "UserInfo" as userinfo {
		iUserAccount : integer, <<FK>
		sNickname : text
		iArticleNumber : integer
		iScore : integer
		iReadingScore : integer
		sRegisterDate : date
		iExistDays : integer
		iMissingDays : integer
		sShortIntroduction : text
	}
	entity "MyCV" as mycv {
		*MyCV_ID : integer, <<PK>>
		*iShows : number
		*User_iAccount :integer, <<FK>>
	}
	entity "SpecificCV" as specificcv {
		*SpecificCV_ID : integer
		*sDescription : text
		ProofAttachment : BLOB
		*TotalCV_ID :integer, <<FK>>
	}

	entity "Attachment" as file {
		File_ID : integer <<PK>>
		sDescription : text
		File : BLOB
		*TotalCV_ID : integer, <<FK>>

	}

	user"1" -- "1"totalcv : has >
	user"1" -- "1..*"mycv : owns >
	totalcv"1" -- "1..*"mycv : produces >
	user"1" -- "1"userinfo : has >
	totalcv"1" -- "0..*"specificcv : contains >
	totalcv"1" -- "0..*"file : possess >

	entity "Board" as board {
		*iBoardNumber : integer, <<PK>>
		*sBoardName : String
	}

	entity "Article" as art {
		*iSerialNumberOfArticle : integer. <<PK>>
		*iBoardNumber : integer, <<FK>>
		*sTitle : String
		*sContent : String
		*iAuthor : integer, <<FK>> ref User.iAccount
	}
	entity "ArticleTopic" as topic {
		*topic_index : integer, <<PK>>
		*sWhichBoard : integer, <<FK>>
		*sTopic : String
	}
	entity "CommentOfArticle" as comment {
		*iSerialNumberOfArticle : <<FK>>
		*iNumber : integer
		*sContentOfComment : text
		*LeftTime : date
	}
	entity "Report" as report {
		*iReportNumber : integer
		*iReportStatus : integer
		iClassification : integer
		*sReasons : text
		*sWho : String
		sCheckResult : String 
	}

	board"1" -- "0..*"art : contains >
	art"1" -- "0..*"comment : contains >
	art"1" -- "1..*"topic : use >
	art"1" -- "0..*"report  : use >
	user"1" -- "0..*"art : owns >

	entity "Inform" as inform {
		*UserAccount :integer, <<FK>>
		*sContent : text
	}

	user"1" -- "0..*"inform : has >

	entity "ActivityGroup" as group {
		*iGroupnumber : integer <<PK>>
		*sGroupTitle : String
		*iStatus : integer
		createdTime : date
	}

	entity "GroupChat" as chat {
		*sMessage : text
		*iGroupNumber :integer, <<FK>>
		*LeftTime : date
		*iWhoLeft : integer <<FK>>
	}
	entity "GroupThreeM" as threeM {
		*sContent : text
		*iGroupNumber :integer, <<FK>>
		*LeftTime : date
		*iWhoLeft : integer <<FK>>
	}
	entity "sGroupSubtitle" as sub {
		*sSubtitle : String
		*iGroupNumber :integer <<FK>>
	}
	entity "UserOfActivityGroup" as member{
		*iUserAccount : int <<FK>>
		*iGroupnumber : int <<FK>>
	}

	group"1" -- "0..*"chat : contains >
	group"1" -- "0..*"threeM : contains >
	group"1" -- "0..*"sub : contains >
	user"0..*" -- "0..*"group : joins >
	(user, group) . member

	@enduml

====================================================================
v0.3 20191018 add many to many between user and activity group
====================================================================

.. uml::

	@startuml
	hide circle
	skinparam linetype ortho

	'change accuont from string to integer and make it primary key
	'change identification to number for which represent a id by 1 and 0 if not possess one.
	entity "User" as user {
		*sName : text
		*iIdentification : 8-digits number, default 00000000
		*iAccount : number <<PK>>
		*sPassword : String
	}
	'bring out attachment to new class
	entity "TotalCV" as totalcv {
		*TotalCV_ID : integer <<PK>>
		*iUserAccount : integer, <<FK>>
		*sName : text
		iStudentID : number
		sNickname : text
		iSex : integer
		sIdentification : String
		sPhoneNumber1 : String
		sPhoneNumber2 : String
		sPhoneNumber3 : String
		sAddressHome : text
		sAddressCompany : text
		sEmail : text
	}
	entity "UserInfo" as userinfo {
		iUserAccount : integer, <<FK>
		sNickname : text
		iArticleNumber : integer
		iScore : integer
		iReadingScore : integer
		sRegisterDate : date
		iExistDays : integer
		iMissingDays : integer
		sShortIntroduction : text
	}
	entity "MyCV" as mycv {
		*MyCV_ID : integer, <<PK>>
		*iShows : number
		*User_iAccount :integer, <<FK>>
	}
	entity "SpecificCV" as specificcv {
		*SpecificCV_ID : integer
		*sDescription : text
		ProofAttachment : BLOB
		*TotalCV_ID :integer, <<FK>>
	}

	entity "Attachment" as file {
		File_ID : integer <<PK>>
		sDescription : text
		File : BLOB
		*TotalCV_ID : integer, <<FK>>

	}

	user"1" -- "1"totalcv : has >
	user"1" -- "1..*"mycv : owns >
	totalcv"1" -- "1..*"mycv : produces >
	user"1" -- "1"userinfo : has >
	totalcv"1" -- "0..*"specificcv : contains >
	totalcv"1" -- "0..*"file : possess >

	entity "Board" as board {
		*iBoardNumber : integer, <<PK>>
		*sBoardName : String
	}

	entity "Article" as art {
		*iSerialNumberOfArticle : integer. <<PK>>
		*iBoardNumber : integer, <<FK>>
		*sTitle : String
		*sContent : String
		*iAuthor : integer, <<FK>> ref User.iAccount
		WhoLikes : List
		sTopic : List
		WhoCollect : List
	}
	entity "ArticleTopic" as topic {
		*topic_index : integer, <<PK>>
		*sWhichBoard : integer, <<FK>>
		*sTopic : String
	}
	entity "CommentOfArticle" as comment {
		*iSerialNumberOfArticle : <<FK>>
		*iNumber : integer
		*sContentOfComment : text
		*LeftTime : date
	}
	entity "Report" as report {
		*iReportNumber : integer
		*iReportStatus : integer
		iClassification : integer
		*sReasons : text
		*sWho : String
		sCheckResult : String 
	}

	board"1" -- "0..*"art : contains >
	art"1" -- "0..*"comment : contains >
	art"1" -- "1..*"topic : use >
	art"1" -- "0..*"report  : use >
	user"1" -- "0..*"art : owns >

	entity "Inform" as inform {
		*UserAccount :integer, <<FK>>
		*sContent : text
	}

	user"1" -- "0..*"inform : has >

	entity "ActivityGroup" as group {
		*iGroupnumber : integer <<PK>>
		*sGroupTitle : String
		*sGroupMember : List
		*iStatus : integer
		createdTime : date
	}

	entity "GroupChat" as chat {
		*sMessage : text
		*iGroupNumber :integer, <<FK>>
		*LeftTime : date
		*iWhoLeft : integer <<FK>>
	}
	entity "GroupThreeM" as threeM {
		*sContent : text
		*iGroupNumber :integer, <<FK>>
		*LeftTime : date
		*iWhoLeft : integer <<FK>>
	}
	entity "sGroupSubtitle" as sub {
		*sSubtitle : String
		*iGroupNumber :integer <<FK>>
	}
	entity "UserOfActivityGroup" as member{
		*iUserAccount : int <<FK>>
		*iGroupnumber : int <<FK>>
	}

	group"1" -- "0..*"chat : contains >
	group"1" -- "0..*"threeM : contains >
	group"1" -- "0..*"sub : contains >
	user"0..*" -- "0..*"group : joins >
	(user, group) . member

	@enduml

====================================================================
v0.2 20191016 change article topic
====================================================================

.. uml::

	@startuml
	hide circle
	skinparam linetype ortho

	'change accuont from string to integer and make it primary key
	'change identification to number for which represent a id by 1 and 0 if not possess one.
	entity "User" as user {
		*sName : text
		*iIdentification : 8-digits number, default 00000000
		*iAccount : number <<PK>>
		*sPassword : String
	}
	'bring out attachment to new class
	entity "TotalCV" as totalcv {
		*TotalCV_ID : integer <<PK>>
		*iUserAccount : integer, <<FK>>
		*sName : text
		iStudentID : number
		sNickname : text
		iSex : integer
		sIdentification : String
		sPhoneNumber1 : String
		sPhoneNumber2 : String
		sPhoneNumber3 : String
		sAddressHome : text
		sAddressCompany : text
		sEmail : text
	}
	entity "UserInfo" as userinfo {
		iUserAccount : integer, <<FK>
		sNickname : text
		iArticleNumber : integer
		iScore : integer
		iReadingScore : integer
		sRegisterDate : date
		iExistDays : integer
		iMissingDays : integer
		sShortIntroduction : text
	}
	entity "MyCV" as mycv {
		*MyCV_ID : integer, <<PK>>
		*iShows : number
		*User_iAccount :integer, <<FK>>
	}
	entity "SpecificCV" as specificcv {
		*SpecificCV_ID : integer
		*sDescription : text
		ProofAttachment : BLOB
		*TotalCV_ID :integer, <<FK>>
	}

	entity "Attachment" as file {
		File_ID : integer <<PK>>
		sDescription : text
		File : BLOB
		*TotalCV_ID : integer, <<FK>>

	}

	user"1" -- "1"totalcv : has >
	user"1" -- "1..*"mycv : owns >
	totalcv"1" -- "1..*"mycv : produces >
	user"1" -- "1"userinfo : has >
	totalcv"1" -- "0..*"specificcv : contains >
	totalcv"1" -- "0..*"file : possess >

	entity "Board" as board {
		*iBoardNumber : integer, <<PK>>
		*sBoardName : String
	}

	entity "Article" as art {
		*iSerialNumberOfArticle : integer. <<PK>>
		*iBoardNumber : integer, <<FK>>
		*sTitle : String
		*sContent : String
		*iAuthor : integer, <<FK>> ref User.iAccount
		WhoLikes : List
		sTopic : List
		WhoCollect : List
	}
	entity "ArticleTopic" as topic {
		*topic_index : integer, <<PK>>
		*sWhichBoard : integer, <<FK>>
		*sTopic : String
	}
	entity "CommentOfArticle" as comment {
		*iSerialNumberOfArticle : <<FK>>
		*iNumber : integer
		*sContentOfComment : text
		*LeftTime : date
	}
	entity "Report" as report {
		*iReportNumber : integer
		*iReportStatus : integer
		iClassification : integer
		*sReasons : text
		*sWho : String
		sCheckResult : String 
	}

	board"1" -- "0..*"art : contains >
	art"1" -- "0..*"comment : contains >
	art"1" -- "1..*"topic : use >
	art"1" -- "0..*"report  : use >
	user"1" -- "0..*"art : owns >

	entity "Inform" as inform {
		*UserAccount :integer, <<FK>>
		*sContent : text
	}

	user"1" -- "0..*"inform : has >

	entity "ActivityGroup" as group {
		*iGroupnumber : integer <<PK>>
		*sGroupTitle : String
		*sGroupMember : List
		*iStatus : integer
		createdTime : date
		*sGroupMember : List
	}

	entity "GroupChat" as chat {
		*sMessage : text
		*iGroupNumber :integer, <<FK>>
		*LeftTime : date
		*iWhoLeft : integer <<FK>>
	}
	entity "GroupThreeM" as threeM {
		*sContent : text
		*iGroupNumber :integer, <<FK>>
		*LeftTime : date
		*iWhoLeft : integer <<FK>>
	}
	entity "sGroupSubtitle" as sub {
		*sSubtitle : String
		*iGroupNumber :integer <<FK>>
	}

	group"1" -- "0..*"member : contains >
	group"1" -- "0..*"chat : contains >
	group"1" -- "0..*"threeM : contains >
	group"1" -- "0..*"sub : contains >
	user"1" -- "0..*"group : joins >

	@enduml

====================================================================
v0.1 20191013 drafted by LKY
====================================================================

.. uml::

	@startuml
	hide circle
	skinparam linetype ortho

	'change accuont from string to integer and make it primary key
	'change identification to number for which represent a id by 1 and 0 if not possess one.
	entity "User" as user {
		*sName : text
		*iIdentification : 8-digits number, default 00000000
		*iAccount : number <<PK>>
		*sPassword : String

	}
	'bring out attachment to new class
	entity "TotalCV" as totalcv {
		*PK : integer <<PK>>
		*sName : text
		sStudentID : number
		sNickname : text
		iSex : integer
		sIdentification : String
		sPhoneNumber1 : String
		sPhoneNumber2 : String
		sPhoneNumber3 : String
		sAddressHome : text
		sAddressCompany : text
		sEmail : text

	}
	entity "UserInfo" as userinfo {
		sNickname : text
		iArticleNumber : integer
		iScore : integer
		iReadingScore : integer
		sRegisterDate : date
		iExistDays : integer
		iMissingDays : integer
		sShortIntroduction : text
	}
	entity "MyCV" as mycv {
		*iShows : number
		*User_iAccount : <<FK>>
	}
	entity "SpecificCV" as specificcv {
		sDescription : text
		blobProofAttachment : BLOB
		*TotalCV_PK : <<FK>>
	}

	entity "Attachment" as file {
		sDescription : text
		file : BLOB
		*TotalCV_PK : <<FK>>

	}

	user"1" -- "1"totalcv : has >
	user"1" -- "1..*"mycv : owns >
	totalcv"1" -- "1..*"mycv : produces >
	user"1" -- "1"userinfo : has >
	totalcv"1" -- "0..*"specificcv : contains >
	totalcv"1" -- "0..*"file : possess >

	entity "Board" as board {
		iBoardNumber : integer, <<PK>>
		sBoardName : String
		Article : List
	}

	entity "Article" as art {
		*iSerialNumberOfArticle : integer. <<PK>>
		*Board_iBoardNumber : <<FK>>
		*sTitle : String
		*sContent : String
		WhoLikes : List
		sTopic : List
		WhoCollect : List
	}
	entity "ArticleTopic" as topic {
		sTopic : String
	}
	entity "CommentOfArticle" as comment {
		*Article_iSerialNumberOfArticle : <<FK>>
		*iNumber : integer
		*sContentOfComment : text
	}
	entity "Report" as report {
		*iReportNumber : integer
		*iReportStatus : integer
		iClassification : integer
		*sReasons : text
		*sWho : String
		sCheckResult : String 
	}

	board"1" -- "0..*"art : contains >
	art"1" -- "0..*"comment : contains >
	art"1" -- "1..*"topic : use >
	art"1" -- "0..*"report  : use >
	user"1" -- "0..*"art : owns >

	entity "Inform" as inform {
		*UserAccount : <<FK>>
		*sContent : text
	}

	user"1" -- "0..*"inform : has >

	entity "Group" as group {
		*iGroupnumber : integer <<PK>>
		*sGroupTitle : String
		*sGroupMember : List
		*iStatus : integer
		sGroupSubtitle : String

	}
	entity "GroupMember" as member {
		*iMemberAccount : integer
		*Group_iGroupNumber : <<FK>>
	}

	entity "GroupChat" as chat {
		*sMessage : text
		*Group_iGroupNumber : <<FK>>
		*Date : date
		*sWho : String
	}
	entity "Group3M" as threeM {
		*sContent : text
		*Group_iGroupNumber : <<FK>>
		*Date : date
		*sWho : String
	}
	entity "sGroupSubtitle" as sub {
		*sSubtitle : String
		*Group_iGroupNumber : <<FK>>
	}

	group"1" -- "0..*"member : contains >
	group"1" -- "0..*"chat : contains >
	group"1" -- "0..*"threeM : contains >
	group"1" -- "0..*"sub : contains >
	user"1" -- "0..*"group : joins >

	@enduml